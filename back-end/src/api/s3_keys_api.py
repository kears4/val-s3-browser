import requests
import os
from flask import request

val_services_url = os.environ['VAL_SERVICES_URL']


def get_keys():
    authorization = request.headers.get('Authorization')
    get_keys_url = val_services_url + '/v1/s3keys/'

    response = requests.get(get_keys_url, headers={
                            'Authorization': authorization,
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'})

    return response.json()
