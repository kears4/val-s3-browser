from routes.bucket_route import bucket_route
from routes.file_route import file_route

from app.flask_app import app
# DO not remove import it's necessary to bootstrap oidc before registering blueprints.
from app.flask_oidc import oidc

print(oidc)

app.register_blueprint(bucket_route, url_prefix='/bucket')
app.register_blueprint(file_route, url_prefix='/file')
