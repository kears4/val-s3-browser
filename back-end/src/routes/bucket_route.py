from flask import Blueprint, jsonify
import s3.s3_proxy as s3_proxy
from app.flask_oidc import oidc

bucket_route = Blueprint('bucket_route', __name__)


@bucket_route.route('/', methods=['GET'])
@oidc.accept_token(require_token=True)
def buckets():
    return jsonify(s3_proxy.get_buckets())


@bucket_route.route('/content/<bucketname>', methods=['GET'])
@oidc.accept_token(require_token=True)
def bucket_content(bucketname):
    return jsonify(s3_proxy.get_bucket_content(bucketname))


@bucket_route.route('/create/<bucketname>', methods=['POST'])
@oidc.accept_token(require_token=True)
def create_bucket(bucketname):
    return jsonify(s3_proxy.create_bucket(bucketname))
