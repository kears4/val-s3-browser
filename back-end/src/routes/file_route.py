from flask import Blueprint, send_file, request
from app.flask_oidc import oidc
import s3.s3_proxy as s3_proxy


file_route = Blueprint('file_route', __name__)


@file_route.route('/download-file-url', methods=['GET'])
@oidc.accept_token(require_token=True)
def download_file_url():
    bucket = request.args.get('bucket')
    file_key = request.args.get('file_key')

    url = s3_proxy.get_download_url(bucket, file_key)
    return url


@file_route.route('/upload-file-url', methods=['GET'])
@oidc.accept_token(require_token=True)
def upload_file_url():
    bucket = request.args.get('bucket')
    file_key = request.args.get('file_key')

    url = s3_proxy.get_upload_url(bucket, file_key)
    return url
