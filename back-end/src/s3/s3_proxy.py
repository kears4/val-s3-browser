import boto3
import os
from api.s3_keys_api import get_keys

s3_host = os.environ['S3_HOST']


def get_resource_connection():
    s3_keys = get_keys()

    return boto3.resource('s3',
                          endpoint_url=s3_host,
                          aws_access_key_id=s3_keys['accessKey'],
                          aws_secret_access_key=s3_keys['secretKey'],
                          config=boto3.session.Config(
                              signature_version='s3v4')
                          )


def get_client_connection():
    s3_keys = get_keys()

    return boto3.client('s3',
                        endpoint_url=s3_host,
                        aws_access_key_id=s3_keys['accessKey'],
                        aws_secret_access_key=s3_keys['secretKey'],
                        config=boto3.session.Config(
                            signature_version='s3v4')
                        )


def get_buckets():
    conn = get_resource_connection()

    buckets = []

    for bucket in conn.buckets.all():
        buckets.append({'name': bucket.name})

    return buckets


def get_bucket_content(bucket_name):
    conn = get_resource_connection()

    contents = []

    for file_content in conn.Bucket(bucket_name).objects.all():
        contents.append({'path': file_content.key})

    return contents


def create_bucket(bucket_name):
    conn = get_resource_connection()
    bucket = conn.Bucket(bucket_name)

    return bucket.create()


def get_download_url(bucket_name, file_key):
    conn = get_client_connection()

    url = conn.generate_presigned_url(
        ClientMethod='get_object',
        Params={
            'Bucket': bucket_name,
            'Key': file_key
        }
    )

    return url


def get_upload_url(bucket_name, file_key):
    conn = get_client_connection()

    url = conn.generate_presigned_url(
        ClientMethod='put_object',
        Params={
            'Bucket': bucket_name,
            'Key': file_key
        }
    )

    return url
