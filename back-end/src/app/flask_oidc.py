from app.flask_app import app
from flask_oidc import OpenIDConnect

oidc = OpenIDConnect(app)
