from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)

app.config.update({
    'SECRET_KEY': 'secret-key',
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': 'config/client_secrets.json',
    'OIDC_OPENID_REALM': 'valeria',
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
    'OIDC-SCOPES': ['openid'],
    'CORS_HEADERS': 'Content-Type'
})
