import BucketContentDto from '../../dataTransferObjects/BucketContentDto';
import FileMapper from '../../mappers/file/FileMapper';
import FolderMapper from '../../mappers/folder/FolderMapper';
import FileModel from '../../models/FileModel';
import FolderModel from '../../models/FolderModel';
import BucketContentFolderGrouper from './BucketContentFolderGrouper';

function get(contentForFolder: BucketContentDto[], currentLevel: number): (FileModel | FolderModel)[] {
    const folderContent = new Array<FileModel | FolderModel>();
    const folderBucketContentGrouped = BucketContentFolderGrouper.getForLevel(contentForFolder, currentLevel);

    const treatedFoldersAtLevel = new Set<string>();

    folderBucketContentGrouped.forEach(contentGrouped => {
        contentGrouped.forEach(item => {
            const itemPathSplit = item.path.split('/');

            // Item is a folder or a file.
            if (itemPathSplit.length === currentLevel + 1) {
                if (item.path[item.path.length - 1] !== '/') {
                    // Item is a file
                    const fileModel = FileMapper.getModel(item);
                    folderContent.push(fileModel);
                    return;
                }
            }

            // Item is a directory.
            const folderName = itemPathSplit[currentLevel];
            if (treatedFoldersAtLevel.has(folderName) || folderName === '') return;

            treatedFoldersAtLevel.add(folderName);

            const newFolderContent = get(contentGrouped, currentLevel + 1);
            const folderModel = FolderMapper.getModel(item, currentLevel, newFolderContent);
            folderContent.push(folderModel);
        });
    });

    return folderContent;
}

export default {
    get
};