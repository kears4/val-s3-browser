import BucketContentDto from '../../dataTransferObjects/BucketContentDto';

function getForLevel(bucketContentsAtLevel: BucketContentDto[],
    currentLevel: number): BucketContentDto[][] {

    const groups = new Array<BucketContentDto[]>();

    const groupsByNameIndex = new Map<string, number>();
    let currentIndex = 0;

    bucketContentsAtLevel.forEach(bucketContent => {
        const bucketSplitByFolder = bucketContent.path.split('/');

        const concernedFolder = bucketSplitByFolder[currentLevel];
        if (concernedFolder === undefined || concernedFolder === '') return;


        if (!groupsByNameIndex.has(concernedFolder)) {
            groupsByNameIndex.set(concernedFolder, currentIndex);
            currentIndex++;
            groups.push([]);
        }

        const groupIndex = groupsByNameIndex.get(concernedFolder) ?? -1;
        groups[groupIndex].push(bucketContent);

    });

    return groups;
}

export default {
    getForLevel
};