import BucketContentDto from '../../dataTransferObjects/BucketContentDto';
import FolderModel from '../../models/FolderModel';
import Constants from '../Constants';
import CurrentLevelFolderContentBuilder from './CurrentLevelFolderContentBuilder';

function getRootFromContent(bucketContents: BucketContentDto[]): FolderModel {
    const rootFolderContent = CurrentLevelFolderContentBuilder.get(bucketContents, 0);
    return new FolderModel(rootFolderContent, Constants.rootFolderName);
}

export default {
    getRootFromContent
};