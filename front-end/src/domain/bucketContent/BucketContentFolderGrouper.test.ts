import BucketContentDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/BucketContentDtoObjectMother';
import BucketContentFolderGrouper from './BucketContentFolderGrouper';

describe('BucketContentFolderGrouper tests', () => {
    it('With two files, same level return files in different groups', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 0);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA],
            [bucketContentB]
        ]);
    });

    it('With two files, same containing folder, both in group', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 0);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA, bucketContentB]
        ]);
    });

    it('With two files, different containing folder, in different groups', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderB/fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 0);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA],
            [bucketContentB]
        ]);
    });

    it('With three files, different containing folder and level, in different groups', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderB/fileB');
        const bucketContentC = BucketContentDtoObjectMother.get('fileC');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB, bucketContentC],
            0);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA],
            [bucketContentB],
            [bucketContentC],
        ]);
    });

    it('With two files, second level, different containing folder, in different groups', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderC/fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 1);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA],
            [bucketContentB]
        ]);
    });

    it('With two files, second level, same containing folder, in same group', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderB/fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 1);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA, bucketContentB]
        ]);
    });

    it('With two files, first level, different second level folder, in same group', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderC/fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 0);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA, bucketContentB]
        ]);
    });

    it('With two files, third level, only files that deep, returns files in different groups', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderB/fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 2);

        // ASSER
        expect(contentGrouped).toEqual([
            [bucketContentA],
            [bucketContentB]
        ]);
    });

    it('With two files, fourth level, no folder/files that deep, returns empty list', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderC/fileB');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA, bucketContentB], 3);

        // ASSER
        expect(contentGrouped).toEqual([]);
    });

    it('With one level folder, at level zero, returns folder', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA], 0);

        // ASSER
        expect(contentGrouped).toEqual([[bucketContentA]]);
    });

    it('With one level folder, at level one, returns nothing', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/');

        // ACT
        const contentGrouped = BucketContentFolderGrouper.getForLevel([bucketContentA], 1);

        // ASSER
        expect(contentGrouped).toEqual([]);
    });
});