import BucketContentDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/BucketContentDtoObjectMother';
import CurrentLevelFolderContentBuilder from './CurrentLevelFolderContentBuilder';

describe('CurrentLevelFolderBuilder tests', () => {
    it('With two files, level 0, return both files', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('fileB');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB], 0);

        // ASSER
        expect(contentGrouped).toEqual([
            { name: 'fileA', path: 'fileA' },
            { name: 'fileB', path: 'fileB' }
        ]);
    });

    it('With two files, same containing folder, returns folder with files', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/fileB');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB], 0);

        // ASSER
        expect(contentGrouped).toEqual([
            {
                name: 'folderA', content: [
                    { name: 'fileA', path: 'folderA/fileA' },
                    { name: 'fileB', path: 'folderA/fileB' }]
            }
        ]);
    });

    it('With two files, different containing folder, returns two folders with files', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderB/fileB');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB], 0);

        // ASSER
        expect(contentGrouped).toEqual([
            {
                name: 'folderA', content: [{ name: 'fileA', path: 'folderA/fileA' }]
            },
            {
                name: 'folderB', content: [{ name: 'fileB', path: 'folderB/fileB' }]
            }
        ]);
    });

    it('With three files, different containing folder and level, structure returned', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderB/fileB');
        const bucketContentC = BucketContentDtoObjectMother.get('fileC');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB, bucketContentC],
            0);

        // ASSER
        expect(contentGrouped).toEqual([
            {
                name: 'folderA', content: [{ name: 'fileA', path: 'folderA/fileA' }]
            },
            {
                name: 'folderB', content: [{ name: 'fileB', path: 'folderB/fileB' }]
            },
            { name: 'fileC', path: 'fileC' }
        ]);
    });

    it('With two files, second level, different containing folder, in different groups', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderC/fileB');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB], 1);

        // ASSER
        expect(contentGrouped).toEqual([
            {
                name: 'folderB', content: [{ name: 'fileA', path: 'folderA/folderB/fileA' }]
            },
            {
                name: 'folderC', content: [{ name: 'fileB', path: 'folderA/folderC/fileB' }]
            }
        ]);
    });

    it('With two files, second level, same containing folder, in same group', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderB/fileB');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB], 1);

        // ASSER
        expect(contentGrouped).toEqual([
            {
                name: 'folderB', content: [
                    { name: 'fileA', path: 'folderA/folderB/fileA' },
                    { name: 'fileB', path: 'folderA/folderB/fileB' }]
            }
        ]);
    });

    it('With two files, third level, only files that deep, returns files in different groups', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderB/fileB');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB], 2);

        // ASSER
        expect(contentGrouped).toEqual([
            { name: 'fileA', path: 'folderA/folderB/fileA' },
            { name: 'fileB', path: 'folderA/folderB/fileB' }
        ]);
    });

    it('With two files, fourth level, no folder/files that deep, returns empty list', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/fileA');
        const bucketContentB = BucketContentDtoObjectMother.get('folderA/folderC/fileB');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA, bucketContentB], 3);

        // ASSER
        expect(contentGrouped).toEqual([]);
    });

    it('With empty folder, first level, folder is gotten without content', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA],
            0);

        // ASSER
        expect(contentGrouped).toEqual([
            {
                name: 'folderA', content: []
            }
        ]);
    });

    it('With empty folder, second level, last folder is gotten without content', () => {
        // ARRANGE
        const bucketContentA = BucketContentDtoObjectMother.get('folderA/folderB/');

        // ACT
        const contentGrouped = CurrentLevelFolderContentBuilder.get([bucketContentA],
            0);

        // ASSER
        expect(contentGrouped).toEqual([
            {
                name: 'folderA', content: [
                    { name: 'folderB', content: [] }
                ]
            }
        ]);
    });
});