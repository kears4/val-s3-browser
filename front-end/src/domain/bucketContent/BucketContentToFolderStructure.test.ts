import BucketContentDto from '../../dataTransferObjects/BucketContentDto';
import FileModel from '../../models/FileModel';
import FolderModel from '../../models/FolderModel';
import BucketContentToFolderStucture from './BucketContentToFolderStucture';
import CurrentLevelFolderContentBuilder from './CurrentLevelFolderContentBuilder';

describe('BucketContentToFolder Tests', () => {
    const BUCKET_CONTENT = new Array<BucketContentDto>();
    const FOLDER_CONTENT = new Array<FileModel | FolderModel>();

    it('Bucket folder root and content is created', () => {
        // ARRANGE
        CurrentLevelFolderContentBuilder.get = jest.fn().mockReturnValue(FOLDER_CONTENT);

        // ACT
        const rootFolder = BucketContentToFolderStucture.getRootFromContent(BUCKET_CONTENT);

        // ASSERT
        expect(rootFolder).toEqual({
            name: 'root',
            content: FOLDER_CONTENT
        });
        expect(CurrentLevelFolderContentBuilder.get).toHaveBeenCalledWith(BUCKET_CONTENT, 0);
    });
});