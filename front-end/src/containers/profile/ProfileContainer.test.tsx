import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import GlobalActions from '../../actions/GlobalActions';
import store from '../../Store';
import RoleModelObjectMother from '../../tests/objectmothers/models/RoleModelObjectMother';
import UserModelObjectMother from '../../tests/objectmothers/models/UserModelObjectMother';
import RolesStateObjectMother from '../../tests/objectmothers/state/RolesStateObjectMother';
import StateObjectMother from '../../tests/objectmothers/state/StateObjectMother';
import UserStateObjectMother from '../../tests/objectmothers/state/UserStateObjectMother';
import { find } from '../../tests/utils/FindHelpers';
import ProfileContainer from './ProfileContainer';

describe('<ProfileContainer />', () => {
    const ADMIN_ROLE = RoleModelObjectMother.get(0, 'ADMIN', 'Admin');
    const COLLABO_ROLE = RoleModelObjectMother.get(1, 'COLLABO', 'Collaborateur');

    const ROLES_STATE = RolesStateObjectMother.getWithRoles([ADMIN_ROLE, COLLABO_ROLE]);

    const USER = UserModelObjectMother.get('', '', '', '', COLLABO_ROLE.idRole);
    const USER_STATE = UserStateObjectMother.getWithUserModel(USER);

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);

        const state = StateObjectMother.getWithUserAndRolesState(USER_STATE, ROLES_STATE);
        store.getState = jest.fn().mockReturnValue(state);
        store.dispatch = jest.fn();
    });

    it('Has user name from store', async () => {
        const wrapper = mount((<Provider store={store}>
            <ProfileContainer />
        </Provider>));

        // ASSERT
        const userName = find(wrapper, 'userName');
        expect(userName.exists()).toBeTruthy();
    });

    it('Has user role name from store', async () => {
        const wrapper = mount((<Provider store={store}>
            <ProfileContainer />
        </Provider>));

        // ASSERT
        const roleName = find(wrapper, 'roleName');
        expect(roleName.exists()).toBeTruthy();
    });
});