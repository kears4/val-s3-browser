import { connect } from 'react-redux';
import Profile from '../../components/profile/Profile';
import { ProfileMapToState } from '../../components/profile/ProfileProps';
import UserSelectors from '../../reducers/user/UserSelectors';
import State from '../../State';


const mapStateToProps = (storeState: State): ProfileMapToState => {
    return {
        user: storeState.user.user,
        roleName: UserSelectors.getUserRoleName(storeState)
    };
};

export default connect(mapStateToProps)(Profile);