import { connect } from 'react-redux';

import State from '../../../../State';
import { Dispatch } from 'redux';
import { BrowseToParentMapToDispatch, BrowseToParentMapToStateProps } from '../../../../components/fileBrowser/menu/browseToParent/BrowseToParentProps';
import FoldersAction from '../../../../reducers/folders/FoldersAction';
import FoldersActions from '../../../../actions/folders/FoldersActions';
import BrowseToParent from '../../../../components/fileBrowser/menu/browseToParent/BrowseToParent';
import FoldersSelectors from '../../../../reducers/folders/FoldersSelectors';

const mapStateToProps = (storeState: State): BrowseToParentMapToStateProps => {
    return {
        disabled: FoldersSelectors.isAtRootFolder(storeState)
    } as BrowseToParentMapToStateProps;
};

const mapDispatchToProps = (dispatch: Dispatch<FoldersAction>): BrowseToParentMapToDispatch => {
    return {
        browseToParent: () => {
            dispatch(FoldersActions.browseToParent());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BrowseToParent);