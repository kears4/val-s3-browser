import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../../Store';
import { find } from '../../../../tests/utils/FindHelpers';
import FoldersActions from '../../../../actions/folders/FoldersActions';
import GlobalActions from '../../../../actions/GlobalActions';
import BrowseToParentContainer from './BrowseToParentContainer';
import FoldersSelectors from '../../../../reducers/folders/FoldersSelectors';

describe('<BrowseToParentContainer />', () => {
    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        FoldersActions.browseToParent = jest.fn().mockReturnValue({ type: '' });
        FoldersSelectors.isAtRootFolder = jest.fn().mockReturnValue(false);

    });

    it('Can browse to parent', async () => {
        // ARRANGE
        const wrapper = mount((<Provider store={store}>
            <BrowseToParentContainer />
        </Provider>));

        const browseToParentButton = find(wrapper, 'browseToParentButton').at(0);

        // ACT
        browseToParentButton.simulate('click');

        // ASSERT
        expect(FoldersActions.browseToParent).toHaveBeenCalled();
    });

    it('Is disabled when at root', () => {
        // ARRANGE
        FoldersSelectors.isAtRootFolder = jest.fn().mockReturnValue(true);

        const wrapper = mount((<Provider store={store}>
            <BrowseToParentContainer />
        </Provider>));

        // ACT
        const browseToParentButton = find(wrapper, 'browseToParentButton').at(0);

        // ASSERT
        expect(browseToParentButton.prop('disabled')).toBeTruthy();
    });
});