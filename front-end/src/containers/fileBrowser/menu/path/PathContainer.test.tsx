import PathContainer from './PathContainer';
import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../../Store';
import GlobalActions from '../../../../actions/GlobalActions';
import { find } from '../../../../tests/utils/FindHelpers';
import FolderModelObjectMother from '../../../../tests/objectmothers/models/FolderModelObjectMother';
import FoldersActions from '../../../../actions/folders/FoldersActions';

describe('<PathContainer />', () => {
    const FOLDER_A = FolderModelObjectMother.get('folderA');
    const FOLDER_B = FolderModelObjectMother.get('folderB');

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
    });

    it('Shows folder path', async () => {
        // ARRANGE
        store.dispatch(FoldersActions.openFolder(FOLDER_A));
        store.dispatch(FoldersActions.openFolder(FOLDER_B));

        // ACT
        const wrapper = mount((<Provider store={store}>
            <PathContainer />
        </Provider>));

        // ASSERT
        expect(find(wrapper, 'folderPath').at(0).text()).toContain('\\folderA\\folderB');
    });
});