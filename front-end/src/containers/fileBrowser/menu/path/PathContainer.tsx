import { connect } from 'react-redux';

import State from '../../../../State';
import Path from '../../../../components/fileBrowser/menu/path/Path';
import { PathMapToStateProps } from '../../../../components/fileBrowser/menu/path/PathProps';

const mapStateToProps = (storeState: State): PathMapToStateProps => {
    return {
        folderPath: storeState.folders.path
    };
};

export default connect(mapStateToProps)(Path);