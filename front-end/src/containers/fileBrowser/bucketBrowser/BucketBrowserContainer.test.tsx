console.warn = jest.fn();

import BucketBrowserContainer from './BucketBrowserContainer';
import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../Store';
import GlobalActions from '../../../actions/GlobalActions';
import { find } from '../../../tests/utils/FindHelpers';
import BucketModelObjectMother from '../../../tests/objectmothers/models/BucketModelObjectMother';
import BucketsStateObjectMother from '../../../tests/objectmothers/state/BucketsStateObjectMother';
import StateObjectMother from '../../../tests/objectmothers/state/StateObjectMother';
import BucketsActionTypes from '../../../actions/types/BucketsActionTypes';

describe('<BucketBrowserContainer />', () => {
    const BUCKET_A = BucketModelObjectMother.get('bucketA');
    const BUCKET_B = BucketModelObjectMother.get('bucketB');

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();
    });

    it('Has buckets from store', async () => {
        // ARRANGE
        const bucketsState = BucketsStateObjectMother.getWithBuckets([BUCKET_A, BUCKET_B]);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.getWithBucketsState(bucketsState));

        // ACT
        const wrapper = mount((<Provider store={store}>
            <BucketBrowserContainer />
        </Provider>));

        // ASSERT
        const bucketsComboBox = find(wrapper, 'bucketsComboBox');
        expect(bucketsComboBox.prop('items')).toEqual([
            { value: BUCKET_A, text: BUCKET_A.name },
            { value: BUCKET_B, text: BUCKET_B.name }
        ]);
    });

    it('Has current active bucket', async () => {
        // ARRANGE
        const bucketsState = BucketsStateObjectMother.getWithActiveBucket(BUCKET_B);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.getWithBucketsState(bucketsState));

        // ACT
        const wrapper = mount((<Provider store={store}>
            <BucketBrowserContainer />
        </Provider>));

        // ASSERT
        const bucketsComboBox = find(wrapper, 'bucketsComboBox');
        expect(bucketsComboBox.prop('selectedValue')).toEqual(BUCKET_B);
    });

    it('Can set active bucket', async () => {
        // ARRANGE
        const wrapper = mount((<Provider store={store}>
            <BucketBrowserContainer />
        </Provider>));

        const bucketsComboBox = find(wrapper, 'bucketsComboBox');

        // ACT
        (bucketsComboBox.instance() as any).handleChange({ target: { value: BUCKET_B } });

        // ASSERT
        expect(store.dispatch).toHaveBeenCalledWith({
            type: BucketsActionTypes.SET_ACTIVE_BUCKET,
            bucket: BUCKET_B
        });
    });
});