import { connect } from 'react-redux';

import State from '../../../State';
import { Dispatch } from 'redux';
import BucketBrowser from '../../../components/fileBrowser/bucketBrowser/BucketBrowser';
import { BucketBrowserMapToDispatch, BucketBrowserMapToStateProps } from '../../../components/fileBrowser/bucketBrowser/BucketBrowserProps';
import BucketsAction from '../../../reducers/buckets/BucketsAction';
import BucketModel from '../../../models/BucketModel';
import BucketsActions from '../../../actions/buckets/BucketsActions';

const mapStateToProps = (storeState: State): BucketBrowserMapToStateProps => {
    return {
        buckets: storeState.buckets.buckets,
        currentBucket: storeState.buckets.activeBucket
    };
};

const mapDispatchToProps = (dispatch: Dispatch<BucketsAction>): BucketBrowserMapToDispatch => {
    return {
        openBucket: (bucket: BucketModel) => {
            BucketsActions.openBucket(dispatch, bucket);
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BucketBrowser);