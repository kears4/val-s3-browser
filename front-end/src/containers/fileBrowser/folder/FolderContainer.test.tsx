import FolderContainer from './FolderContainer';
import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../Store';
import GlobalActions from '../../../actions/GlobalActions';
import { find } from '../../../tests/utils/FindHelpers';
import FoldersActions from '../../../actions/folders/FoldersActions';
import FolderModelObjectMother from '../../../tests/objectmothers/models/FolderModelObjectMother';

describe('<FolderContainer />', () => {
    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        FoldersActions.openFolder = jest.fn().mockReturnValue({ type: '' });
    });

    it('Shows folder', async () => {
        // ARRANGE
        const folder = FolderModelObjectMother.get();

        // ACT
        const wrapper = mount((<Provider store={store}>
            <FolderContainer folder={folder} />
        </Provider>));

        // ASSERT
        expect(find(wrapper, `folder-${folder.name}`).exists()).toBeTruthy();
    });

    it('Can set active folder', async () => {
        // ARRANGE
        const folder = FolderModelObjectMother.get();

        const wrapper = mount((<Provider store={store}>
            <FolderContainer folder={folder} />
        </Provider>));

        const browseButton = find(wrapper, 'browseButton').at(0);

        // ACT
        browseButton.simulate('click');

        // ASSERT
        expect(FoldersActions.openFolder).toHaveBeenCalledWith(folder);
    });
});