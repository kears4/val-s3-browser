import { connect } from 'react-redux';

import State from '../../../State';
import { Dispatch } from 'redux';
import { FolderMapToDispatch, FolderMapToStateProps } from '../../../components/fileBrowser/folder/FolderProps';
import FoldersAction from '../../../reducers/folders/FoldersAction';
import FolderModel from '../../../models/FolderModel';
import FoldersActions from '../../../actions/folders/FoldersActions';
import Folder from '../../../components/fileBrowser/folder/Folder';

const mapStateToProps = (storeState: State, props: FolderMapToStateProps): FolderMapToStateProps => {
    return props;
};

const mapDispatchToProps = (dispatch: Dispatch<FoldersAction>): FolderMapToDispatch => {
    return {
        browse: (folder: FolderModel) => {
            dispatch(FoldersActions.openFolder(folder));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Folder);