import { connect } from 'react-redux';

import State from '../../../State';
import FolderStructure from '../../../components/fileBrowser/folderStructure/FolderStructure';
import FolderStructureProps from '../../../components/fileBrowser/folderStructure/FolderStructureProps';
import FoldersSelectors from '../../../reducers/folders/FoldersSelectors';

const mapStateToProps = (storeState: State): FolderStructureProps => {
    return {
        folder: FoldersSelectors.getCurrentFolder(storeState)
    };
};

export default connect(mapStateToProps)(FolderStructure);