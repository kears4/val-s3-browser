console.error = jest.fn();

import FolderStructure from './FolderStructureContainer';
import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../Store';
import GlobalActions from '../../../actions/GlobalActions';
import { find } from '../../../tests/utils/FindHelpers';
import FoldersActions from '../../../actions/folders/FoldersActions';
import FolderModelObjectMother from '../../../tests/objectmothers/models/FolderModelObjectMother';

describe('<FolderStructureContainer />', () => {
    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
    });

    it('Shows active folder content', async () => {
        // ARRANGE
        const innerFolderA = FolderModelObjectMother.get('innerA');
        const innerFolderB = FolderModelObjectMother.get('innerB');

        const folder = FolderModelObjectMother.getWithContent([innerFolderA, innerFolderB]);
        store.dispatch(FoldersActions.openFolder(folder));

        // ACT
        const wrapper = mount((<Provider store={store}>
            <FolderStructure />
        </Provider>));

        // ASSERT
        expect(find(wrapper, `folder-${innerFolderA.name}`).exists()).toBeTruthy();
        expect(find(wrapper, `folder-${innerFolderB.name}`).exists()).toBeTruthy();
    });
});