import { connect } from 'react-redux';

import State from '../../../State';
import { FileContainerProps, FileProps } from '../../../components/fileBrowser/file/FileProps';
import File from '../../../components/fileBrowser/file/File';

const mapStateToProps = (storeState: State, props: FileContainerProps): FileProps => {
    return {
        file: props.file,
        bucket: storeState.buckets.activeBucket,
    };
};

export default connect(mapStateToProps)(File);