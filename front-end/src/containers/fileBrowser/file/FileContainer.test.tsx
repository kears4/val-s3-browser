import FileContainer from './FileContainer';
import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import store from '../../../Store';
import GlobalActions from '../../../actions/GlobalActions';
import FileModelObjectMother from '../../../tests/objectmothers/models/FileModelObjectMother';
import FileServices from '../../../services/bucketContent/BucketContentServices';
import { find } from '../../../tests/utils/FindHelpers';
import BucketModelObjectMother from '../../../tests/objectmothers/models/BucketModelObjectMother';
import BucketsStateObjectMother from '../../../tests/objectmothers/state/BucketsStateObjectMother';
import StateObjectMother from '../../../tests/objectmothers/state/StateObjectMother';

describe('<FileContainer />', () => {
    const ACTIVE_BUCKET = BucketModelObjectMother.get();

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        FileServices.downloadFile = jest.fn();

        const bucketState = BucketsStateObjectMother.getWithActiveBucket(ACTIVE_BUCKET);
        const state = StateObjectMother.getWithBucketsState(bucketState);

        store.getState = jest.fn().mockReturnValue(state);
    });

    it('Shows file', async () => {
        // ARRANGE
        const file = FileModelObjectMother.get();

        // ACT
        const wrapper = mount((<Provider store={store}>
            <FileContainer file={file} />
        </Provider>));

        // ASSERT
        expect(find(wrapper, `file-${file.name}`).exists()).toBeTruthy();
    });
});