import { State } from 'history';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import BucketsActions from '../../../../actions/buckets/BucketsActions';
import ViewBucketButton from '../../../../components/buckets/bucketList/viewBucketButton/ViewBucketButton';
import { ViewBucketButtonContainerProps, ViewBucketButtonMapToDispatchProps, ViewBucketButtonMapToStateProps } from '../../../../components/buckets/bucketList/viewBucketButton/ViewBucketButtonProps';
import BucketModel from '../../../../models/BucketModel';
import BucketsAction from '../../../../reducers/buckets/BucketsAction';


const mapStateToProps = (storeState: State, props: ViewBucketButtonContainerProps): ViewBucketButtonMapToStateProps => {
    return {
        bucket: props.bucket
    };
};

const mapDispatchToProps = (dispatch: Dispatch<BucketsAction>): ViewBucketButtonMapToDispatchProps => {
    return {
        openBucket: (bucket: BucketModel) => {
            BucketsActions.openBucket(dispatch, bucket);
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(ViewBucketButton);