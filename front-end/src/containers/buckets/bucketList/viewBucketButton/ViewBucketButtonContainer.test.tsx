console.error = jest.fn();

import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import BucketsActions from '../../../../actions/buckets/BucketsActions';
import GlobalActions from '../../../../actions/GlobalActions';
import store from '../../../../Store';
import BucketModelObjectMother from '../../../../tests/objectmothers/models/BucketModelObjectMother';
import StateObjectMother from '../../../../tests/objectmothers/state/StateObjectMother';
import { find } from '../../../../tests/utils/FindHelpers';
import ViewBucketButtonContainer from './ViewBucketButtonContainer';

describe('<ViewBucketButtonContainer />', () => {
    const BUCKET = BucketModelObjectMother.get('BucketA');

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();
    });

    it('When click on view bucket button, openBucket action is called', async () => {
        // ARRANGE
        BucketsActions.openBucket = jest.fn();

        // ACT
        const wrapper = mount((<Provider store={store}>
            <ViewBucketButtonContainer bucket={BUCKET} />
        </Provider>));

        // ASSERT
        const bucketIntroCreate = find(wrapper, 'viewBucketButton');
        expect(bucketIntroCreate.exists()).toBeTruthy();
    });
});