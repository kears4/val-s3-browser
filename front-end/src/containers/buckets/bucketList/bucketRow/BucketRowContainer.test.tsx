console.error = jest.fn();

import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import BucketsActions from '../../../../actions/buckets/BucketsActions';
import GlobalActions from '../../../../actions/GlobalActions';
import store from '../../../../Store';
import BucketModelObjectMother from '../../../../tests/objectmothers/models/BucketModelObjectMother';
import StateObjectMother from '../../../../tests/objectmothers/state/StateObjectMother';
import { find } from '../../../../tests/utils/FindHelpers';
import BucketRowContainer from './BucketRowContainer';

describe('<BucketRowContainer />', () => {
    const BUCKET = BucketModelObjectMother.get('BucketA');

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();
    });

    it('Has view bucket row button', async () => {
        // ARRANGE
        BucketsActions.openBucket = jest.fn();

        // ACT
        const wrapper = mount((<Provider store={store}>
            <BucketRowContainer bucket={BUCKET} />
        </Provider>));

        // ASSERT
        const viewBucketRowButton = find(wrapper, 'viewBucketRowButton');
        expect(viewBucketRowButton.exists()).toBeTruthy();
    });
});