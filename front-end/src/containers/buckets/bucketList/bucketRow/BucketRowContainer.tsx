import { State } from 'history';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import BucketsActions from '../../../../actions/buckets/BucketsActions';
import BucketRow from '../../../../components/buckets/bucketList/bucketRow/BucketRow';
import { BucketRowContainerProps, BucketRowMapToDispatchProps, BucketRowMapToStateProps } from '../../../../components/buckets/bucketList/bucketRow/BucketRowProps';
import BucketModel from '../../../../models/BucketModel';
import BucketsAction from '../../../../reducers/buckets/BucketsAction';


const mapStateToProps = (storeState: State, props: BucketRowContainerProps): BucketRowMapToStateProps => {
    return {
        bucket: props.bucket
    };
};

const mapDispatchToProps = (dispatch: Dispatch<BucketsAction>): BucketRowMapToDispatchProps => {
    return {
        openBucket: (bucket: BucketModel) => {
            BucketsActions.openBucket(dispatch, bucket);
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(BucketRow);