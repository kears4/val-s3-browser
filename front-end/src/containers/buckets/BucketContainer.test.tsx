console.error = jest.fn();

import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import GlobalActions from '../../actions/GlobalActions';
import store from '../../Store';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import BucketsStateObjectMother from '../../tests/objectmothers/state/BucketsStateObjectMother';
import StateObjectMother from '../../tests/objectmothers/state/StateObjectMother';
import { find } from '../../tests/utils/FindHelpers';
import BucketContainer from './BucketContainer';

describe('<BucketContainer />', () => {
    const BUCKET_A = BucketModelObjectMother.get('BucketA');
    const BUCKET_B = BucketModelObjectMother.get('BucketB');

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();
    });

    it('With no buckets, has bucketIntroCreate component', async () => {
        // ARRANGE
        const bucketsState = BucketsStateObjectMother.get();
        store.getState = jest.fn().mockReturnValue(StateObjectMother.getWithBucketsState(bucketsState));

        // ACT
        const wrapper = mount((<Provider store={store}>
            <BucketContainer />
        </Provider>));

        // ASSERT
        const bucketIntroCreate = find(wrapper, 'bucketIntroCreate');
        expect(bucketIntroCreate.exists()).toBeTruthy();
    });

    it('With buckets, has bucketList component with buckets', async () => {
        // ARRANGE
        const bucketsState = BucketsStateObjectMother.getWithBuckets([BUCKET_A, BUCKET_B]);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.getWithBucketsState(bucketsState));

        // ACT
        const wrapper = mount((<Provider store={store}>
            <BucketContainer />
        </Provider>));

        // ASSERT
        const bucketIntroCreate = find(wrapper, 'bucketsList');
        expect(bucketIntroCreate.prop('buckets')).toEqual(bucketsState.buckets);
    });
});