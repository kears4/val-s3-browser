console.error = jest.fn();

import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import BucketsActions from '../../../../actions/buckets/BucketsActions';
import GlobalActions from '../../../../actions/GlobalActions';
import store from '../../../../Store';
import StateObjectMother from '../../../../tests/objectmothers/state/StateObjectMother';
import { find } from '../../../../tests/utils/FindHelpers';
import AddBucketDialogButtonContainer from './AddBucketDialogButtonContainer';

describe('<AddBucketDialogButtonContainer />', () => {
    let onCloseDialog: jest.Mock;
    let onSetBucketCreationError: jest.Mock;

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();

        onCloseDialog = jest.fn();
        onSetBucketCreationError = jest.fn();
    });

    it('When press on button, createBucket action is called', async () => {
        // ARRANGE
        const bucketName = 'my-bucket';
        BucketsActions.createBucket = jest.fn();

        const wrapper = mount((<Provider store={store}>
            <AddBucketDialogButtonContainer bucketName={bucketName} closeDialog={onCloseDialog}
                setBucketCreationError={onSetBucketCreationError} />
        </Provider>));

        const createBucketButton = find(wrapper, 'addBucketButton').at(0);

        // ACT
        createBucketButton.simulate('click');

        // ASSERT
        expect(BucketsActions.createBucket).toHaveBeenCalledWith(store.dispatch, bucketName);
    });
});