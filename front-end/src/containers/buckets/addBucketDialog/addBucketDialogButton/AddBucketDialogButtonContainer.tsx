import { connect } from 'react-redux';

import { Dispatch } from 'redux';

import AddBucketDialogButton from '../../../../components/buckets/addBucketDialog/addBucketDialogButton/AddBucketDialogButton';
import {
    AddBucketDialogButtonContainerProps,
    AddBucketDialogButtonMapToDispatch,
    AddBucketDialogButtonMapToStateProps
} from '../../../../components/buckets/addBucketDialog/addBucketDialogButton/AddBucketDialogButtonProps';
import BucketsAction from '../../../../reducers/buckets/BucketsAction';
import BucketsActions from '../../../../actions/buckets/BucketsActions';
import State from '../../../../State';

const mapStateToProps = (storeState: State, props: AddBucketDialogButtonContainerProps)
    : AddBucketDialogButtonMapToStateProps => {
    return {
        bucketName: props.bucketName,
        closeDialog: props.closeDialog,
        setBucketCreationError: props.setBucketCreationError,
        disabled: props.disabled
    };
};

const mapDispatchToProps = (dispatch: Dispatch<BucketsAction>): AddBucketDialogButtonMapToDispatch => {
    return {
        createBucket: (bucketName: string) => BucketsActions.createBucket(dispatch, bucketName)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddBucketDialogButton);