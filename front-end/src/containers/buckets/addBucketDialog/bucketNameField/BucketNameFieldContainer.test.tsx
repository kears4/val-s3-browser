console.error = jest.fn();

import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import GlobalActions from '../../../../actions/GlobalActions';
import store from '../../../../Store';
import StateObjectMother from '../../../../tests/objectmothers/state/StateObjectMother';
import { find } from '../../../../tests/utils/FindHelpers';
import BucketNameFieldContainer from './BucketNameFieldContainer';

describe('<BucketNameFieldContainer />', () => {
    let onSetNewBucketName: jest.Mock;

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);
        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();
    });

    it('It has bucket name field', async () => {
        // ARRANGE
        onSetNewBucketName = jest.fn();

        // ACT
        const wrapper = mount((<Provider store={store}>
            <BucketNameFieldContainer setNewBucketName={onSetNewBucketName} />
        </Provider>));

        // ASSERT
        expect(find(wrapper, 'bucketNameField').exists()).toBeTruthy();
    });
});