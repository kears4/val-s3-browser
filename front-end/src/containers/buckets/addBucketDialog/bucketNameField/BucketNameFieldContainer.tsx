import { connect } from 'react-redux';
import BucketNameField from '../../../../components/buckets/addBucketDialog/bucketNameField/BucketNameField';
import { BucketNameFieldContainerProps, BucketNameFieldProps } from '../../../../components/buckets/addBucketDialog/bucketNameField/BucketNameFieldProps';
import State from '../../../../State';

const mapStateToProps = (storeState: State, props: BucketNameFieldContainerProps)
    : BucketNameFieldProps => {
    return {
        buckets: storeState.buckets.buckets,
        setNewBucketName: props.setNewBucketName
    };
};

export default connect(mapStateToProps)(BucketNameField);