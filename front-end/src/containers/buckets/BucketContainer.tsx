import { connect } from 'react-redux';
import Buckets from '../../components/buckets/Buckets';
import { BucketsMapToStateProps } from '../../components/buckets/BucketsProps';
import State from '../../State';


const mapStateToProps = (storeState: State): BucketsMapToStateProps => {
    return {
        buckets: storeState.buckets.buckets
    };
};

export default connect(mapStateToProps)(Buckets);