import { connect } from 'react-redux';

import State from '../../State';
import { Dispatch } from 'redux';
import UploadButton from '../../components/upload/UploadButton';
import { UploadButtonMapToDispatchProps, UploadButtonMapToStateProps } from '../../components/upload/UploadButtonProps';
import FoldersAction from '../../reducers/folders/FoldersAction';
import FoldersActions from '../../actions/folders/FoldersActions';
import FoldersState from '../../reducers/folders/FoldersState';
import BucketModel from '../../models/BucketModel';
import BucketsSelectors from '../../reducers/buckets/BucketsSelectors';

const mapStateToProps = (storeState: State): UploadButtonMapToStateProps => {
    return {
        folderState: storeState.folders,
        bucket: storeState.buckets.activeBucket,
        disabled: !BucketsSelectors.isOnActiveBucket(storeState)
    };
};

const mapDispatchToProps = (dispatch: Dispatch<FoldersAction>): UploadButtonMapToDispatchProps => {
    return {
        uploadFile: (folderState: FoldersState, bucket: BucketModel, file: File) => {
            FoldersActions.uploadFile(dispatch, folderState, bucket, file);
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadButton);