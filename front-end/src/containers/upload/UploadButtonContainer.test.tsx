import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import GlobalActions from '../../actions/GlobalActions';
import store from '../../Store';
import BucketsStateObjectMother from '../../tests/objectmothers/state/BucketsStateObjectMother';
import StateObjectMother from '../../tests/objectmothers/state/StateObjectMother';
import { find } from '../../tests/utils/FindHelpers';
import UploadButtonContainer from './UploadButtonContainer';
import BucketsSelectors from '../../reducers/buckets/BucketsSelectors';


describe('<UploadButtonContainer />', () => {
    const ACTIVE_BUCKET = BucketModelObjectMother.get();

    beforeEach(() => {
        GlobalActions.resetStore(store.dispatch);

        const bucketState = BucketsStateObjectMother.getWithActiveBucket(ACTIVE_BUCKET);
        const state = StateObjectMother.getWithBucketsState(bucketState);

        store.getState = jest.fn().mockReturnValue(state);
    });

    it('Has upload button', async () => {
        const wrapper = mount((<Provider store={store}>
            <UploadButtonContainer />
        </Provider>));

        expect(find(wrapper, 'uploadButton').exists()).toBeTruthy();
    });

    it('With no active bucket, upload button is disabled', async () => {
        BucketsSelectors.isOnActiveBucket = jest.fn().mockReturnValue(false);

        const wrapper = mount((<Provider store={store}>
            <UploadButtonContainer />
        </Provider>));

        expect(find(wrapper, 'uploadButton').at(0).prop('disabled')).toBeTruthy();
    });

    it('With active bucket, upload button is not disabled', async () => {
        BucketsSelectors.isOnActiveBucket = jest.fn().mockReturnValue(true);

        const wrapper = mount((<Provider store={store}>
            <UploadButtonContainer />
        </Provider>));

        expect(find(wrapper, 'uploadButton').at(0).prop('disabled')).toBeFalsy();
    });
});