export default {
    backendUrl: process.env.REACT_APP_BACKEND_URL ? process.env.REACT_APP_BACKEND_URL : 'http://localhost:5000',
    valServicesUrl: process.env.REACT_APP_VAL_SERVICES_URL ? process.env.REACT_APP_VAL_SERVICES_URL : 'https://services.valeria.science/val',
};