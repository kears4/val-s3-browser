import React from 'react';
import { CssBaseline, ThemeProvider } from '@material-ui/core';

import Routes from './components/Routes';
import MainLayout from './components/layouts/MainLayout';
import { BrowserRouter as Router } from 'react-router-dom';
// import darkTheme from './themes/darkTheme';
import lightTheme from './themes/lightTheme';
import AppProps from './AppProps';

const appContentStyle = {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    paddingTop: 64,
    paddingLeft: 256
};


export default function App(props: AppProps) {

    return (
        <ThemeProvider theme={lightTheme}>
            <CssBaseline />

            <Router>
                <MainLayout logout={props.logout} />
                <div style={appContentStyle}>
                    <Routes />
                </div>
            </Router>

        </ThemeProvider>
    );
}