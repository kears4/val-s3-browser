import { colors } from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles';
import shadows from './shadows';
import typography from './typography';

// A custom theme for this app
const theme = createTheme({
    palette: {
        background: {
            default: '#F4F6F8',
            paper: colors.common.white
        },
        primary: {
            contrastText: '#ffffff',
            main: '#519872'
        },
        secondary: {
            contrastText: '#519872',
            main: '#172b4d'
        },
        text: {
            primary: '#172b4d',
            secondary: '#6b778c'
        }
    },
    shadows,
    typography,
});

export default theme;
