import { createTheme } from '@material-ui/core/styles';
import shadows from './shadows';
import typography from './typography';

// A custom theme for this app
const theme = createTheme({
    palette: {
        background: {
            default: '#161616',
            paper: '#444444'
        },
        primary: {
            contrastText: '#ECDBBA',
            main: '#346751'
        },
        secondary: {
            contrastText: '#EDEDED',
            main: '#ECDBBA'
        },
        text: {
            primary: '#ECDBBA',
            secondary: '#ECDBBA'
        },
        divider: 'rgba(255, 255, 255, 0.12)'
    },
    shadows,
    typography
});

export default theme;
