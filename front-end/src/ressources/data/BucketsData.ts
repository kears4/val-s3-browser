import BucketDto from '../../dataTransferObjects/BucketDto';

const bucketA: BucketDto = { name: 'bucketA' };
const bucketB: BucketDto = { name: 'bucketB' };
const bucketC: BucketDto = { name: 'bucketC' };


export default {
    buckets: [bucketA, bucketB, bucketC]
};