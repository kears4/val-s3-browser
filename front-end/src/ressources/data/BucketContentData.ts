import BucketContentDto from '../../dataTransferObjects/BucketContentDto';

const bucketContentData: BucketContentDto[] = [
    { path: 'Analyse données pour DTI.R' },
    { path: 'Truchon_Manon_13-11-19 copie.pdf' },
    { path: 'resultsQBST-codeQuestion.xlsx' },
    { path: 'test/FolderA/' },
    { path: 'test/FolderB/' },
    { path: 'test/FolderB/Digital Snow.wav' },
    { path: 'test/FolderB/FolderC/' },
    { path: 'test/test.txt' },
];

export default {
    bucketContentData
};