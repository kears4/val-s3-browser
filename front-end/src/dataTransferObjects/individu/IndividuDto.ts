import RoleIndividuDto from './RoleIndividuDto';

export default interface IndividuDto {
    adresseCourriel: string;
    idValeria: string;
    prenom: string;
    nom: string;

    rolesIndividu: RoleIndividuDto[];
}