export default interface PagedResultDto<TResultType> {
    resultats: TResultType[];
}