export default interface RoleDto {
    idRole: number;
    codeRole: string;
    nom: string;
}