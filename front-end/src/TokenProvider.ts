let tokenInstance = '';

function registerToken(token: string) {
    tokenInstance = token;
}

function getUserToken(): string {
    return tokenInstance;
}

export default {
    registerToken,
    getUserToken
};