import IndividuDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/individu/IndividuDtoObjectMother';
import IndividuMapper from './UserMapper';

describe('UserMapper tests', () => {
    it('maps individuDto to user', () => {
        // ARRANGE
        const individuDto = IndividuDtoObjectMother.get();

        // ACT
        const model = IndividuMapper.get(individuDto);

        // ASSERT
        expect(model.valeriaId).toEqual(individuDto.idValeria);
        expect(model.email).toEqual(individuDto.adresseCourriel);
        expect(model.firstName).toEqual(individuDto.prenom);
        expect(model.lastName).toEqual(individuDto.nom);
        expect(model.roleId).toEqual(individuDto.rolesIndividu[0].idRole);
    });
});