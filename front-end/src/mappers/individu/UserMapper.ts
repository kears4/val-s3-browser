import IndividuDto from '../../dataTransferObjects/individu/IndividuDto';
import IndividuModel from '../../models/UserModel';

function get(individuDto: IndividuDto) {
    return new IndividuModel(individuDto.idValeria,
        individuDto.adresseCourriel,
        individuDto.prenom,
        individuDto.nom,
        individuDto.rolesIndividu[0].idRole);
}

export default {
    get
};