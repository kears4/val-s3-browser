import BucketDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/BucketDtoObjectMother';
import BucketMapper from './BucketMapper';

describe('BucketMapper tests', () => {
    it('maps bucket dto content to model', () => {
        // ARRANGE
        const bucketDto = BucketDtoObjectMother.get('bucket');

        // ACT
        const model = BucketMapper.get(bucketDto);

        // ASSERT
        expect(model.name).toEqual(bucketDto.name);
    });
});