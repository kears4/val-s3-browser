import BucketDto from '../../dataTransferObjects/BucketDto';
import BucketModel from '../../models/BucketModel';


function get(bucketDto: BucketDto) {
    return new BucketModel(bucketDto.name);
}

export default {
    get
};