import RoleDto from '../../dataTransferObjects/role/RoleDto';
import RoleModel from '../../models/RoleModel';


function get(roleDto: RoleDto) {
    return new RoleModel(roleDto.idRole, roleDto.codeRole, roleDto.nom);
}

export default {
    get
};