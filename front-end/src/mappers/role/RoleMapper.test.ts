import RoleDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/role/RoleDtoObjectMother';
import RoleMapper from './RoleMapper';

describe('RoleMapper tests', () => {
    it('maps roleDto to model', () => {
        // ARRANGE
        const role = RoleDtoObjectMother.get();

        // ACT
        const model = RoleMapper.get(role);

        // ASSERT
        expect(model.idRole).toEqual(role.idRole);
        expect(model.codeRole).toEqual(role.codeRole);
        expect(model.nom).toEqual(role.nom);
    });
});