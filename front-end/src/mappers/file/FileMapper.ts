import BucketContentDto from '../../dataTransferObjects/BucketContentDto';
import FileModel from '../../models/FileModel';

function getModel(bucketContent: BucketContentDto) {
    const contentByFolder = bucketContent.path.split('/');
    const fileName = contentByFolder[contentByFolder.length - 1];

    return new FileModel(fileName, bucketContent.path);
}

export default {
    getModel
};