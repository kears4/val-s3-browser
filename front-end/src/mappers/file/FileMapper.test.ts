import BucketContentDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/BucketContentDtoObjectMother';
import FileMapper from './FileMapper';

describe('FileMapper tests', () => {
    it('Returns file name when path is one level deep', () => {
        // ARRANGE
        const bucketContent = BucketContentDtoObjectMother.get('file');

        // ACT
        const fileModel = FileMapper.getModel(bucketContent);

        // ASSERT
        expect(fileModel.name).toEqual('file');
        expect(fileModel.path).toEqual('file');
    });

    it('Returns file name when path is three level deep', () => {
        // ARRANGE
        const bucketContent = BucketContentDtoObjectMother.get('folderA/folderB/file');

        // ACT
        const fileModel = FileMapper.getModel(bucketContent);

        // ASSERT
        expect(fileModel.name).toEqual('file');
        expect(fileModel.path).toEqual('folderA/folderB/file');
    });
});