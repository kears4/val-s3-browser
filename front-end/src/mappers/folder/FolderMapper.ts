import BucketContentDto from '../../dataTransferObjects/BucketContentDto';
import FileModel from '../../models/FileModel';
import FolderModel from '../../models/FolderModel';

function getModel(bucketContent: BucketContentDto,
    currentLevel: number,
    folderContent: (FileModel | FolderModel)[]): FolderModel {

    const folders = bucketContent.path.split('/');

    return new FolderModel(folderContent, folders[currentLevel]);
}

export default {
    getModel
};