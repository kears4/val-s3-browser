import Folder from '../../components/fileBrowser/folder/Folder';
import FileModel from '../../models/FileModel';
import FolderModel from '../../models/FolderModel';
import BucketContentDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/BucketContentDtoObjectMother';
import FolderMapper from './FolderMapper';

describe('FolderMapper tests', () => {
    const BUCKET_CONTENT = BucketContentDtoObjectMother.get('folderA/folderB/file.ext');
    const FOLDER_CONTENT = Array<FileModel | FolderModel>();

    it('on level 0 return first folder with content', () => {
        const folderModel = FolderMapper.getModel(BUCKET_CONTENT, 0, FOLDER_CONTENT);

        expect(folderModel.name).toEqual('folderA');
        expect(folderModel.content).toEqual(FOLDER_CONTENT);
    });

    it('on level 1 return second folder with content', () => {
        const folderModel = FolderMapper.getModel(BUCKET_CONTENT, 1, FOLDER_CONTENT);

        expect(folderModel.name).toEqual('folderB');
        expect(folderModel.content).toEqual(FOLDER_CONTENT);
    });
});