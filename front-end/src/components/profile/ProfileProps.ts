import UserModel from '../../models/UserModel';

export interface ProfileMapToState {
    user: UserModel;
    roleName: string;
}

export type ProfileProps = ProfileMapToState;