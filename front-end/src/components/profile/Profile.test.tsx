import { ShallowWrapper, shallow } from 'enzyme';
import React from 'react';
import UserModelObjectMother from '../../tests/objectmothers/models/UserModelObjectMother';
import { find } from '../../tests/utils/FindHelpers';
import Profile from './Profile';

describe('<Profile />', () => {
    const USER = UserModelObjectMother.get();
    const ROLE_NAME = 'Admin';

    let wrapper: ShallowWrapper<Profile>;

    beforeEach(() => {
        wrapper = shallow(<Profile user={USER} roleName={ROLE_NAME} />);
    });

    it('has profile picture', () => {
        const profilePicture = find(wrapper, 'profilePicture');
        expect(profilePicture.exists()).toBeTruthy();
    });

    it('has user name', () => {
        const userName = find(wrapper, 'userName');
        expect(userName.text()).toEqual(`${USER.firstName} ${USER.lastName}`);
    });

    it('has valeria id', () => {
        const valeriaId = find(wrapper, 'valeriaId');
        expect(valeriaId.text()).toEqual(`ID VALERIA: ${USER.valeriaId}`);
    });

    it('has role name', () => {
        const roleName = find(wrapper, 'roleName');
        expect(roleName.text()).toEqual(ROLE_NAME);
    });
});