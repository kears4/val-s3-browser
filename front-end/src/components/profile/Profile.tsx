import React from 'react';
import {
    Box, Divider, Typography,
} from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { ProfileProps } from './ProfileProps';

export default class Profile extends React.Component<ProfileProps> {
    render() {
        const userName = `${this.props.user.firstName} ${this.props.user.lastName}`;
        const valeriaId = `ID VALERIA: ${this.props.user.valeriaId}`;
        const roleName = this.props.roleName;

        return (
            <>
                <Box
                    sx={{
                        alignItems: 'center',
                        display: 'flex',
                        flexDirection: 'column',
                        p: 2
                    }}
                >
                    <AccountCircleIcon fontSize='large' color='inherit' data-test-id='profilePicture' />

                    <Typography
                        color='textPrimary'
                        variant='h5'
                        data-test-id='userName'
                    >
                        {userName}
                    </Typography>

                    <Typography
                        color='textSecondary'
                        variant='body2'
                        data-test-id='valeriaId'
                    >
                        {valeriaId}
                    </Typography>

                    <Typography
                        color='textSecondary'
                        variant='body2'
                        data-test-id='roleName'
                    >
                        {roleName}
                    </Typography>
                </Box>

                <Divider />
            </>
        );
    }
}