export default interface AddBucketDialogState {
    bucketName: string;
    isAddButtonDisabled: boolean;
    serverError: string | undefined;
}