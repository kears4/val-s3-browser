console.error = jest.fn();

import React from 'react';

import { mount, shallow, ShallowWrapper } from 'enzyme';
import { find } from '../../../tests/utils/FindHelpers';
import AddBucketDialogProps from './AddBucketDialogProps';
import AddBucketDialog from './AddBucketDialog';


describe('<AddBucketDialog />', () => {
    let wrapper: ShallowWrapper<AddBucketDialogProps>;

    let onCloseDialog: jest.Mock;

    beforeEach(() => {
        onCloseDialog = jest.fn();
        wrapper = shallow(<AddBucketDialog closeDialog={onCloseDialog} isOpen={true} />);
    });

    it('hasModal open from props', () => {
        const addBucketDialog = find(wrapper, 'addBucketDialog');
        expect(addBucketDialog.exists()).toBeTruthy();
    });

    it('has title', () => {
        const title = find(wrapper, 'addBucketDialogTitle');
        expect(title.exists()).toBeTruthy();
    });

    it('has description', () => {
        const description = find(wrapper, 'addBucketDialogDescription');
        expect(description.exists()).toBeTruthy();
    });

    it('has name field', () => {
        const nameField = find(wrapper, 'addBucketDialogNameField');
        expect(nameField.exists()).toBeTruthy();
    });

    it('when click on cancel button, dialog is closed', () => {
        const cancelButton = find(wrapper, 'addBucketDialogCancelButton');
        cancelButton.simulate('click');

        expect(onCloseDialog).toHaveBeenCalled();
    });
});
