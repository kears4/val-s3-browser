import {
    Alert,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Typography
} from '@material-ui/core';
import React from 'react';
import { useState } from 'react';
import AddBucketDialogButtonContainer from '../../../containers/buckets/addBucketDialog/addBucketDialogButton/AddBucketDialogButtonContainer';
import BucketNameFieldContainer from '../../../containers/buckets/addBucketDialog/bucketNameField/BucketNameFieldContainer';
import AddBucketDialogProps from './AddBucketDialogProps';
import AddBucketDialogState from './AddBucketDialogState';

export default function AddBucketDialog(props: AddBucketDialogProps) {
    const [state, setState] = useState<AddBucketDialogState>({
        bucketName: '',
        isAddButtonDisabled: true,
        serverError: undefined
    });

    const handleClose = () => {
        props.closeDialog();
    };

    const setNewBucketName = (bucketName: string, hasError: boolean) => {
        setState({ bucketName, isAddButtonDisabled: hasError, serverError: state.serverError });
    };

    const setServerError = (newServerError: string | undefined) => {
        setState({
            serverError: newServerError,
            bucketName: state.bucketName,
            isAddButtonDisabled: state.isAddButtonDisabled,
        });
    };

    const serverError = state.serverError
        ? (<Alert severity='error' sx={{ marginBottom: '16px' }}>{state.serverError}</Alert>)
        : (<></>);

    return (
        <Dialog
            open={props.isOpen}
            onClose={handleClose}
            aria-labelledby='form-dialog-title'
            data-test-id='addBucketDialog'
        >
            <DialogTitle id='form-dialog-title' sx={{ backgroundColor: 'primary.main', color: 'primary.contrastText' }}>
                <Typography variant='h3' data-test-id='addBucketDialogTitle'>
                    Ajouter un compartiment
                </Typography>
            </DialogTitle>
            <DialogContent >
                {serverError}

                <DialogContentText>
                    <Typography data-test-id='addBucketDialogDescription'>
                        Veuillez choisir un nom pour votre compartiment
                    </Typography>
                </DialogContentText>
                <BucketNameFieldContainer
                    setNewBucketName={setNewBucketName}
                    data-test-id='addBucketDialogNameField' />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color='inherit' data-test-id='addBucketDialogCancelButton'>
                    Annuler
                </Button>
                <AddBucketDialogButtonContainer
                    bucketName={state.bucketName}
                    disabled={state.isAddButtonDisabled}
                    closeDialog={handleClose}
                    setBucketCreationError={setServerError}
                    data-test-id='addBucketDialogAddButton' />
            </DialogActions>
        </Dialog>
    );
}