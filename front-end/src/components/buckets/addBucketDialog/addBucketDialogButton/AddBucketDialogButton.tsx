import { Button } from '@material-ui/core';
import React from 'react';
import { AddBucketDialogButtonProps } from './AddBucketDialogButtonProps';

export default function AddBucketDialogButton(props: AddBucketDialogButtonProps) {
    const createBucket = async () => {
        const error = await props.createBucket(props.bucketName);
        if (!error) {
            props.closeDialog();
            return;
        }

        props.setBucketCreationError(error);
    };

    return (
        <Button onClick={createBucket} color='primary' variant='contained' disabled={props.disabled}
            data-test-id='addBucketButton'>
            Ajouter
        </Button>
    );
}