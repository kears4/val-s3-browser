export interface AddBucketDialogButtonContainerProps {
    bucketName: string;
    closeDialog: () => void;
    setBucketCreationError: (error: string) => void;
    disabled?: boolean;
}
export interface AddBucketDialogButtonMapToStateProps {
    bucketName: string;
    closeDialog: () => void;
    setBucketCreationError: (error: string) => void;
    disabled?: boolean;
}

export interface AddBucketDialogButtonMapToDispatch {
    createBucket: (bucketName: string) => Promise<string | undefined>;
}

export type AddBucketDialogButtonProps = AddBucketDialogButtonMapToStateProps & AddBucketDialogButtonMapToDispatch;