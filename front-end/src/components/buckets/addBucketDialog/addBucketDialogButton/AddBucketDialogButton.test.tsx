console.error = jest.fn();

import React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import { find } from '../../../../tests/utils/FindHelpers';
import AddBucketDialogButton from './AddBucketDialogButton';
import { when } from 'jest-when';
import { AddBucketDialogButtonProps } from './AddBucketDialogButtonProps';


describe('<AddBucketButton />', () => {
    const BUCKET_NAME = 'my-bucket';

    let wrapper: ShallowWrapper<AddBucketDialogButtonProps>;

    let onCloseDialog: jest.Mock;
    let onSetBucketCreationError: jest.Mock;
    let onCreateBucket: jest.Mock;

    beforeEach(() => {
        onCloseDialog = jest.fn();
        onSetBucketCreationError = jest.fn();
        onCreateBucket = jest.fn();

        wrapper = shallow(<AddBucketDialogButton
            bucketName={BUCKET_NAME}
            closeDialog={onCloseDialog}
            setBucketCreationError={onSetBucketCreationError}
            createBucket={onCreateBucket}
        />);
    });

    it('has button by default not disabled.', () => {
        const addBucketDialogAddButton = find(wrapper, 'addBucketButton');
        expect(addBucketDialogAddButton.prop('disabled')).toBeFalsy();
    });

    it('when disabled, has button not disabled.', () => {
        wrapper = shallow(<AddBucketDialogButton bucketName={BUCKET_NAME}
            closeDialog={onCloseDialog}
            setBucketCreationError={onSetBucketCreationError}
            createBucket={onCreateBucket}
            disabled={true} />);

        const addBucketDialogAddButton = find(wrapper, 'addBucketButton');
        expect(addBucketDialogAddButton.prop('disabled')).toBeTruthy();
    });

    it('when click on button, bucket is created with no error, error is not propagated and dialog is closed', async () => {
        // ARRANGE
        const addBucketDialogAddButton = find(wrapper, 'addBucketButton');

        when(onCreateBucket as any)
            .calledWith(BUCKET_NAME)
            .mockReturnValue(Promise.resolve(undefined));

        // ACT
        await addBucketDialogAddButton.simulate('click');

        // ASSERT
        expect(onCreateBucket).toHaveBeenCalledWith(BUCKET_NAME);
        expect(onCloseDialog).toHaveBeenCalled();

        expect(onSetBucketCreationError).not.toHaveBeenCalled();
    });

    it('when click on button, bucket is created with error, error is propagated and dialog is not closed', async () => {
        // ARRANGE
        const addBucketDialogAddButton = find(wrapper, 'addBucketButton');

        when(onCreateBucket as any)
            .calledWith(BUCKET_NAME)
            .mockReturnValue(Promise.resolve('error'));

        // ACT
        await addBucketDialogAddButton.simulate('click');

        // ASSERT
        expect(onCreateBucket).toHaveBeenCalledWith(BUCKET_NAME);
        expect(onCloseDialog).not.toHaveBeenCalled();

        expect(onSetBucketCreationError).toHaveBeenCalledWith('error');
    });
});
