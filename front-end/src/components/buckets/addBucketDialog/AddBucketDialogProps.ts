export default interface AddBucketDialogProps {
    isOpen: boolean;
    closeDialog: () => void;
}