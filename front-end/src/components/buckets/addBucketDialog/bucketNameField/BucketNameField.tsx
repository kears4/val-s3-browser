import { TextField } from '@material-ui/core';
import React, { useState } from 'react';
import { BucketNameFieldProps } from './BucketNameFieldProps';
import BucketNameFieldState from './BucketNameFieldState';
import BucketNameFieldValidator from './BucketNameFieldValidator';

export default function BucketNameField(props: BucketNameFieldProps) {
    const [state, setState] = useState<BucketNameFieldState>({ errorText: '' });

    const onTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = event.target.value.trim();
        const validationResult = BucketNameFieldValidator.validate(newValue, props.buckets);

        props.setNewBucketName(newValue, !validationResult.isValid);
        setState({ errorText: validationResult.errorMessage });
    };

    return (
        <TextField
            autoFocus
            margin='dense'
            label='Nom compartiment'
            fullWidth
            error={state.errorText.length > 0}
            helperText={state.errorText}
            onChange={onTextChange}
            data-test-id='bucketNameField'
        />
    );
}