console.error = jest.fn();

import React from 'react';

import { ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import BucketNameField from './BucketNameField';
import { find } from '../../../../tests/utils/FindHelpers';
import BucketNameFieldErrors from './BucketNameFieldErrors';
import { specialCharsWithoutHyphen } from '../../../../helpers/Chars';
import { BucketNameFieldProps } from './BucketNameFieldProps';
import BucketModelObjectMother from '../../../../tests/objectmothers/models/BucketModelObjectMother';
import { List } from 'immutable';


describe('<AddBucketDialog />', () => {
    let wrapper: ShallowWrapper<BucketNameFieldProps>;
    let onSetNewBucketName: jest.Mock;

    const BUCKET_A = BucketModelObjectMother.get('bucket-a');
    const BUCKET_B = BucketModelObjectMother.get('bucket-b');

    const BUCKETS = List([BUCKET_A, BUCKET_B]);

    beforeEach(() => {
        onSetNewBucketName = jest.fn();
        wrapper = shallow(<BucketNameField setNewBucketName={onSetNewBucketName} buckets={BUCKETS} />);
    });

    it('has text field', () => {
        const field = find(wrapper, 'bucketNameField');
        expect(field.exists()).toBeTruthy();
    });

    it('with no inputted value, has no error', () => {
        const field = find(wrapper, 'bucketNameField');

        expect(field.prop('error')).toBeFalsy();
        expect(field.prop('helperText')).toEqual('');
    });

    it('when change value, bucket name is set', () => {
        const field = find(wrapper, 'bucketNameField');
        field.simulate('change', getTextChangeEvent('new-value'));

        expect(onSetNewBucketName).toHaveBeenCalledWith('new-value', false);
    });

    describe('validations', () => {
        let field: ShallowWrapper | ReactWrapper<any, any, any>;

        beforeEach(() => {
            field = find(wrapper, 'bucketNameField');
        });

        describe('is not valid', () => {
            it('when lower than 3 characters, has error', () => {
                field.simulate('change', getTextChangeEvent('a'.repeat(2)));
                expectHasError('a'.repeat(2), BucketNameFieldErrors.LOWER_THAN_THREE_CHARS);
            });

            it('when over 63 characters, has error', () => {
                field.simulate('change', getTextChangeEvent('a'.repeat(64)));
                expectHasError('a'.repeat(64), BucketNameFieldErrors.OVER_63_CHARACTERS);
            });

            for (const specialChar of specialCharsWithoutHyphen) {
                it(`With specialChar ${specialChar}, has error`, () => {
                    const specialCharTextPadded = `a${specialChar.repeat(3)}a`;
                    field.simulate('change', getTextChangeEvent(specialCharTextPadded));
                    expectHasError(specialCharTextPadded, BucketNameFieldErrors.HAS_SPECIAL_CHARS_THAT_ARE_NOT_HYPHENS);
                });
            }

            it('when has upper case character, has error', () => {
                field.simulate('change', getTextChangeEvent('A'.repeat(4)));
                expectHasError('A'.repeat(4), BucketNameFieldErrors.HAS_UPPERCASE_LETTERS);
            });

            it('when has hyphen at start, has error', () => {
                field.simulate('change', getTextChangeEvent('-abc'));
                expectHasError('-abc', BucketNameFieldErrors.HAS_HYPHEN_AT_START);
            });

            it('when has hyphen at end, has error', () => {
                field.simulate('change', getTextChangeEvent('abc-'));
                expectHasError('abc-', BucketNameFieldErrors.HAS_HYPHEN_AT_END);
            });

            it('when has already existing bucket name, has error', () => {
                field.simulate('change', getTextChangeEvent(BUCKET_B.name));
                expectHasError(BUCKET_B.name, BucketNameFieldErrors.BUCKET_ALREADY_EXISTS);
            });

            function expectHasError(bucketName: string, errorText: string) {
                field = find(wrapper, 'bucketNameField'); // Re-update field state to get new props

                expect(field.prop('error')).toBeTruthy();
                expect(field.prop('helperText')).toEqual(errorText);

                expect(onSetNewBucketName).toHaveBeenCalledWith(bucketName, true);
            }
        });

        describe('is valid', () => {
            it('when has only lower chars, has no error', () => {
                field.simulate('change', getTextChangeEvent('abc'));
                expectHasNotError('abc');
            });

            it('when has only numbers, has no error', () => {
                field.simulate('change', getTextChangeEvent('1234'));
                expectHasNotError('1234');
            });

            it('when has only lower chars and numbers, has no error', () => {
                field.simulate('change', getTextChangeEvent('abc123'));
                expectHasNotError('abc123');
            });

            it('when has only lower chars, numbers and hyphen, has no error', () => {
                field.simulate('change', getTextChangeEvent('abc-123'));
                expectHasNotError('abc-123');
            });

            function expectHasNotError(bucketName: string) {
                field = find(wrapper, 'bucketNameField'); // Re-update field state to get new props

                expect(field.prop('error')).toBeFalsy();
                expect(field.prop('helperText')).toEqual('');

                expect(onSetNewBucketName).toHaveBeenCalledWith(bucketName, false);
            }
        });
    });

    function getTextChangeEvent(text: string) {
        return {
            target: { value: text }
        } as React.ChangeEvent<HTMLInputElement>;
    }
});
