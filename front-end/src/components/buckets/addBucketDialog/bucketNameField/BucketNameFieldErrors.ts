export default {
    LOWER_THAN_THREE_CHARS: 'Le nom doit contenir au moins 3 caractères',
    OVER_63_CHARACTERS: 'Le nom doit contenur au plus de 63 caractères',
    HAS_UPPERCASE_LETTERS: 'Le nom ne doit pas contenir de majuscule(s)',
    HAS_SPECIAL_CHARS_THAT_ARE_NOT_HYPHENS: "Le nom ne peux comporter aucuns caractères spéciaux à l'exception du tiret -",
    HAS_HYPHEN_AT_START: 'Le nom doit commencer par une lettre minuscule ou un numéro',
    HAS_HYPHEN_AT_END: 'Le nom doit terminer par une lettre minuscule ou un numéro',
    BUCKET_ALREADY_EXISTS: 'Ce nom de compartiment existe déjà'
};