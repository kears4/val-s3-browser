import { List } from 'immutable';
import { specialCharsWithoutHyphen } from '../../../../helpers/Chars';
import BucketModel from '../../../../models/BucketModel';
import BucketNameFieldErrors from './BucketNameFieldErrors';

function getInvalidResult(errorMessage: string) {
    return {
        isValid: false,
        errorMessage
    };
}

function validate(input: string, buckets: List<BucketModel>) {
    if (input.length < 3) return getInvalidResult(BucketNameFieldErrors.LOWER_THAN_THREE_CHARS);
    if (input.length > 63) return getInvalidResult(BucketNameFieldErrors.OVER_63_CHARACTERS);

    if (specialCharsWithoutHyphen.some(scwh => input.includes(scwh)))
        return getInvalidResult(BucketNameFieldErrors.HAS_SPECIAL_CHARS_THAT_ARE_NOT_HYPHENS);

    if (input !== input.toLocaleLowerCase()) return getInvalidResult(BucketNameFieldErrors.HAS_UPPERCASE_LETTERS);

    if (input[0] === '-') return getInvalidResult(BucketNameFieldErrors.HAS_HYPHEN_AT_START);
    if (input[input.length - 1] === '-') return getInvalidResult(BucketNameFieldErrors.HAS_HYPHEN_AT_END);

    if (buckets.some(b => b.name === input)) return getInvalidResult(BucketNameFieldErrors.BUCKET_ALREADY_EXISTS);

    return {
        isValid: true,
        errorMessage: ''
    };
}

export default {
    validate
};