import { List } from 'immutable';
import BucketModel from '../../../../models/BucketModel';

export interface BucketNameFieldContainerProps {
    setNewBucketName: (name: string, hasError: boolean) => void;
}

export interface BucketNameFieldMapStateToProps {
    buckets: List<BucketModel>;
}

export type BucketNameFieldProps = BucketNameFieldContainerProps & BucketNameFieldMapStateToProps;