import { List } from 'immutable';
import BucketModel from '../../models/BucketModel';

export interface BucketsMapToStateProps {
    buckets: List<BucketModel>;
}

// tslint:disable-next-line:no-empty-interface
export interface BucketsMapToDispatch {
}

export type BucketsProps = BucketsMapToStateProps & BucketsMapToDispatch;