import { shallow } from 'enzyme';
import { List } from 'immutable';
import React from 'react';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import { find } from '../../tests/utils/FindHelpers';
import Buckets from './Buckets';

describe('<Buckets />', () => {
    const BUCKET_A = BucketModelObjectMother.get('BucketA');
    const BUCKET_B = BucketModelObjectMother.get('BucketB');

    const BUCKETS = List([BUCKET_A, BUCKET_B]);

    it('With no buckets, has bucketIntroCreate component', async () => {
        // ARRANGE
        const wrapper = shallow((<Buckets buckets={List([])} />));

        // ACT
        const bucketIntroCreate = find(wrapper, 'bucketIntroCreate');

        // ASSERT
        expect(bucketIntroCreate.exists()).toBeTruthy();
    });

    it('With buckets, has bucketList component with buckets', async () => {
        // ARRANGE
        const wrapper = shallow((<Buckets buckets={BUCKETS} />));

        // ACT
        const bucketIntroCreate = find(wrapper, 'bucketsList');

        // ASSERT
        expect(bucketIntroCreate.prop('buckets')).toEqual(BUCKETS);
    });
});