import { Card } from '@material-ui/core';
import React from 'react';
import BucketIntroCreate from './bucketIntroCreate/BucketIntroCreate';
import BucketList from './bucketList/BucketList';
import { BucketsProps } from './BucketsProps';

export default class Buckets extends React.Component<BucketsProps> {
    render() {
        const bucketsContent = this.props.buckets.count() > 0
            ? (<BucketList buckets={this.props.buckets} data-test-id='bucketsList' />)
            : (<BucketIntroCreate data-test-id='bucketIntroCreate' />);

        return (<>
            <Card>
                {bucketsContent}
            </Card>
        </>);
    }
}
