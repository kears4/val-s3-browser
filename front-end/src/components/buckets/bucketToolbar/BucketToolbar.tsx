import React from 'react';
import { Box } from '@material-ui/core';
import AddBucketButton from '../addBucketButton/AddBucketButton';

export default function BucketToolbar() {
	return (<>
		<Box >
			<Box
				sx={{
					display: 'flex',
					justifyContent: 'flex-end'
				}}>

				<AddBucketButton data-test-id='addBucketButton' />
			</Box>
		</Box>
	</>);
}
