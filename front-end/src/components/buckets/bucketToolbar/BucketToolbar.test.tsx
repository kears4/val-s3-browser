import React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import BucketToolbar from './BucketToolbar';
import { find } from '../../../tests/utils/FindHelpers';


describe('<BucketToolbar />', () => {
    let wrapper: ShallowWrapper<AnalyserNode, any, any>;

    beforeEach(() => {
        wrapper = shallow(<BucketToolbar />);
    });

    it('has addBucketButton', () => {
        const addBucketButton = find(wrapper, 'addBucketButton');
        expect(addBucketButton.exists()).toBeTruthy();
    });
});
