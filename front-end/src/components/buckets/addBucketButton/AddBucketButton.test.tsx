console.error = jest.fn();

import React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import { find } from '../../../tests/utils/FindHelpers';
import AddBucketButton from './AddBucketButton';
import AddBucketButtonState from './AddBucketButtonState';
import { mount } from 'enzyme';
import AddBucketButtonProps from './AddBucketButtonProps';
import store from '../../../Store';
import StateObjectMother from '../../../tests/objectmothers/state/StateObjectMother';
import { Provider } from 'react-redux';


describe('<AddBucketButton />', () => {
    let wrapper: ShallowWrapper<AddBucketButtonProps, AddBucketButtonState>;

    beforeEach(() => {
        wrapper = shallow(<AddBucketButton />);
    });

    it('has add bucket label', () => {
        const addBucketButton = find(wrapper, 'addBucketButton');
        expect(addBucketButton.text()).toContain('Ajouter compartiment');
    });

    it('has upload icon', () => {
        const downloadIcon = find(wrapper, 'addBucketIcon');
        expect(downloadIcon.exists()).toBeTruthy();
    });

    it('By default is dialog open is false', () => {
        const addBucketDialog = find(wrapper, 'addBucket');
        expect(addBucketDialog.prop('isOpen')).toBeFalsy();
    });

    it('When press on addBucketButton dialog is open', () => {
        const addBucketButton = find(wrapper, 'addBucketButton');
        addBucketButton.simulate('click');

        const addBucketDialog = find(wrapper, 'addBucket');
        expect(addBucketDialog.prop('isOpen')).toBeTruthy();
    });

    it('Can close dialog once opened', () => {
        // ARRANGE
        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();

        const component = mount(<Provider store={store}>
            <AddBucketButton />
        </Provider>);

        const addBucketButton = find(component, 'addBucketButton').at(0);
        addBucketButton.simulate('click');

        const cancelButton = find(component, 'addBucketDialogCancelButton').at(0);

        // ACT
        cancelButton.simulate('click').at(0);

        // ASSERT
        const addBucketDialog = find(component, 'addBucket');
        expect(addBucketDialog.prop('isOpen')).toBeFalsy();
    });
});
