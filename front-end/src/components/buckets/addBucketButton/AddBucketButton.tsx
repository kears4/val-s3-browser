import { Button, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import AddBucketButtonProps from './AddBucketButtonProps';
import AddBucketButtonState from './AddBucketButtonState';
import AddBucketDialog from '../addBucketDialog/AddBucketDialog';

export default function AddBucketButton(props: AddBucketButtonProps) {
    const [state, setState] = useState<AddBucketButtonState>({ isDialogOpen: false });

    const handleOpenDialog = () => {
        setState({ isDialogOpen: true });
    };

    const handleCloseDialog = () => {
        setState({ isDialogOpen: false });
    };

    return (<>
        <Button
            color='primary'
            variant='contained'
            onClick={handleOpenDialog}
            data-test-id='addBucketButton'
        >
            <AddCircleIcon data-test-id='addBucketIcon' />
            <Typography sx={{ marginLeft: '10px' }} variant='button'>Ajouter compartiment</Typography>
        </Button>

        <AddBucketDialog isOpen={state.isDialogOpen} closeDialog={handleCloseDialog}
            data-test-id='addBucket' />
    </>);
}
