import { shallow } from 'enzyme';
import React from 'react';
import { find } from '../../../tests/utils/FindHelpers';
import BucketIntroCreate from './BucketIntroCreate';

describe('<BucketIntroCreate />', () => {

    it('Has bucket intro create icon', async () => {
        // ARRANGE
        const wrapper = shallow((<BucketIntroCreate />));

        // ACT
        const bucketIntroCreateIcon = find(wrapper, 'bucketIntroCreateIcon');

        // ASSERT
        expect(bucketIntroCreateIcon.exists()).toBeTruthy();
    });

    it('Has bucket intro create text', async () => {
        // ARRANGE
        const wrapper = shallow((<BucketIntroCreate />));

        // ACT
        const bucketIntroCreateText = find(wrapper, 'bucketIntroCreateText');

        // ASSERT
        expect(bucketIntroCreateText.exists()).toBeTruthy();
    });

    it('Has add bucket button', async () => {
        // ARRANGE
        const wrapper = shallow((<BucketIntroCreate />));

        // ACT
        const bucketIntroCreate = find(wrapper, 'addBucketButton');

        // ASSERT
        expect(bucketIntroCreate.exists()).toBeTruthy();
    });
});