import { Box, Grid, Typography } from '@material-ui/core';
import React from 'react';
import AddBucketButton from '../addBucketButton/AddBucketButton';
import StorageIcon from '@material-ui/icons/Storage';

export default function BucketIntroCreate() {
    return (
        <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            pb: 3,
            padding: '16px'
        }}>

            <Grid
                container
                spacing={2}
                sx={{ justifyContent: 'center' }}
            >
                <Grid
                    item
                    sx={{
                        justifyContent: 'center',
                        display: 'flex'
                    }}
                >
                    <Typography align='center'>
                        <StorageIcon fontSize='large' data-test-id='bucketIntroCreateIcon' />
                    </Typography>
                </Grid>

                <Grid
                    item
                    sx={{
                        justifyContent: 'center',
                        display: 'flex'
                    }}
                >
                    <Typography align='center' sx={{ width: '72%' }} data-test-id='bucketIntroCreateText'>
                        Les compartiments s3 vous permettent de stocker de façon sécuritaire des données de tout type d'objets (documents, images, vidéos, etc.)
                        Ces données sont par la suite accessibles à vous et à vos collègues, peu importe où vous êtes situés.
                        Commencez à partager des données en créer un compartiment avec le bouton suivant:
                    </Typography>
                </Grid>

                <Grid
                    item
                    sx={{
                        justifyContent: 'center',
                        display: 'flex'
                    }}
                >
                    <AddBucketButton data-test-id='addBucketButton' />
                </Grid>
            </Grid>
        </Box>
    );
}