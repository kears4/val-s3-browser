import { ReactWrapper, mount } from 'enzyme';
import React from 'react';
import BucketModelObjectMother from '../../../../tests/objectmothers/models/BucketModelObjectMother';
import { find } from '../../../../tests/utils/FindHelpers';
import ViewBucketButton from './ViewBucketButton';
import { createMemoryHistory } from 'history';

import { Router } from 'react-router-dom';
import { ViewBucketButtonProps } from './ViewBucketButtonProps';

describe('<ViewBucketButton />', () => {
    const BUCKET = BucketModelObjectMother.get('Bucket A');

    let onOpenBucket: jest.Mock;
    let pushSpy: any;

    let wrapper: ReactWrapper<ViewBucketButtonProps>;


    beforeEach(() => {
        onOpenBucket = jest.fn();

        const history = createMemoryHistory();
        pushSpy = jest.spyOn(history, 'push'); // or 'replace', 'goBack', etc.

        wrapper = mount((<Router history={history}>
            <ViewBucketButton bucket={BUCKET} openBucket={onOpenBucket} />
        </Router>));
    });

    it('on click, opens bucket and redirects to buckets page', () => {
        // ARRANGE
        const openBucketButton = find(wrapper, 'viewBucketButton').at(0);

        // ACT
        openBucketButton.simulate('click');

        // ASSERT
        expect(onOpenBucket).toHaveBeenCalledWith(BUCKET);
        expect(pushSpy).toHaveBeenCalledWith('/files');
    });
});