import React from 'react';
import { IconButton } from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { ViewBucketButtonProps } from './ViewBucketButtonProps';
import { useHistory } from 'react-router-dom';

export default function ViewBucketButton(props: ViewBucketButtonProps) {
    const history = useHistory();

    const handleClick = () => {
        props.openBucket(props.bucket);
        history.push('/files');
    };

    return (<>
        <IconButton color='inherit' onClick={handleClick} data-test-id='viewBucketButton'>
            <VisibilityIcon />
        </IconButton>
    </>);
}