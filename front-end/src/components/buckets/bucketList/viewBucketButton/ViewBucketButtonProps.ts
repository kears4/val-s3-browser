import BucketModel from '../../../../models/BucketModel';

export interface ViewBucketButtonContainerProps {
    bucket: BucketModel;
}

// tslint:disable-next-line:no-empty-interface
export interface ViewBucketButtonMapToStateProps {
    bucket: BucketModel;
}

export interface ViewBucketButtonMapToDispatchProps {
    openBucket: (bucket: BucketModel) => void;
}

export type ViewBucketButtonProps = ViewBucketButtonContainerProps
    & ViewBucketButtonMapToStateProps
    & ViewBucketButtonMapToDispatchProps;