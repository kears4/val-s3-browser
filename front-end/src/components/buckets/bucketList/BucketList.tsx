import { Box, Table, TableBody, TableCell, TableHead, Typography } from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';
import React from 'react';
import BucketListProps from './BucketListProps';
import BucketRowContainer from '../../../containers/buckets/bucketList/bucketRow/BucketRowContainer';

export default class BucketList extends React.Component<BucketListProps> {
    render() {
        const bucketRows = this.props.buckets.map((b, i) => (
            <BucketRowContainer
                bucket={b}
                key={`bucketRow${i}`}
                data-test-id={`bucketRow${i}`}
                data-test-group='bucketRows'
            />
        ));

        return (<>
            <PerfectScrollbar>
                <Box sx={{ minWidth: 1050 }}>
                    <Table>
                        <TableHead>
                            <TableCell sx={{ width: '32px' }}>
                            </TableCell>
                            <TableCell>
                                <Typography variant='h4'>Nom</Typography>
                            </TableCell>
                            <TableCell align='right'>
                            </TableCell>
                        </TableHead>

                        <TableBody>
                            {bucketRows}
                        </TableBody>
                    </Table>
                </Box>
            </PerfectScrollbar>
        </>);
    }
}