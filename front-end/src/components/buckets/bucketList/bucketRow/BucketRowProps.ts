import BucketModel from '../../../../models/BucketModel';

export interface BucketRowContainerProps {
    bucket: BucketModel;
}

// tslint:disable-next-line:no-empty-interface
export interface BucketRowMapToStateProps {
    bucket: BucketModel;
}

export interface BucketRowMapToDispatchProps {
    openBucket: (bucket: BucketModel) => void;
}

export type BucketRowProps = BucketRowContainerProps & BucketRowMapToStateProps & BucketRowMapToDispatchProps;