import { Button, TableCell, TableRow } from '@material-ui/core';
import React from 'react';
import ViewBucketButtonContainer from '../../../../containers/buckets/bucketList/viewBucketButton/ViewBucketButtonContainer';
import { BucketRowProps } from './BucketRowProps';
import { useHistory } from 'react-router-dom';
import BucketEditMenu from '../bucketEditMenu/BucketEditMenu';

export default function BucketRow(props: BucketRowProps) {
    const history = useHistory();

    const handleClick = () => {
        props.openBucket(props.bucket);
        history.push('/files');
    };

    return (
        <>
            <TableRow>
                <TableCell>
                    <ViewBucketButtonContainer bucket={props.bucket} data-test-id='viewBucketButton' />
                </TableCell>

                <TableCell data-test-id='bucketRowName'>
                    <Button onClick={handleClick} sx={{ textTransform: 'none' } as any} color='inherit'
                        data-test-id='viewBucketRowButton'>

                        {props.bucket.name}
                    </Button>
                </TableCell>

                <TableCell align='right' data-test-id='bucketRowEditMenu'>
                    <BucketEditMenu />
                </TableCell>
            </TableRow>
        </>);
}