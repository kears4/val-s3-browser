console.error = jest.fn();

import { mount, ReactWrapper } from 'enzyme';
import { createMemoryHistory } from 'history';
import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import store from '../../../../Store';
import BucketModelObjectMother from '../../../../tests/objectmothers/models/BucketModelObjectMother';
import StateObjectMother from '../../../../tests/objectmothers/state/StateObjectMother';
import { find } from '../../../../tests/utils/FindHelpers';
import BucketRow from './BucketRow';
import { BucketRowProps } from './BucketRowProps';

describe('<BucketRow />', () => {
    const BUCKET = BucketModelObjectMother.get();

    let onOpenBucket: jest.Mock;
    let pushSpy: any;

    let wrapper: ReactWrapper<BucketRowProps>;

    beforeEach(() => {
        onOpenBucket = jest.fn();

        const history = createMemoryHistory();
        pushSpy = jest.spyOn(history, 'push');

        store.getState = jest.fn().mockReturnValue(StateObjectMother.get());
        store.dispatch = jest.fn();

        wrapper = mount((<Provider store={store}>
            <Router history={history}>
                <BucketRow bucket={BUCKET} openBucket={onOpenBucket} />
            </Router>
        </Provider>
        ));
    });

    it('Has view bucket button', () => {
        const viewBucketButton = find(wrapper, 'viewBucketButton').at(0);
        expect(viewBucketButton.prop('bucket')).toBe(BUCKET);
    });

    it('Has bucket name', () => {
        const name = find(wrapper, 'bucketRowName').at(0);
        expect(name.text()).toEqual(BUCKET.name);
    });

    it('Has edit menu', () => {
        const editMenu = find(wrapper, 'bucketRowEditMenu').at(0);
        expect(editMenu.exists()).toBeTruthy();
    });

    it('on click bucket name, opens bucket and redirects to buckets page', () => {
        // ARRANGE
        const openBucketButton = find(wrapper, 'viewBucketRowButton').at(0);

        // ACT
        openBucketButton.simulate('click');

        // ASSERT
        expect(onOpenBucket).toHaveBeenCalledWith(BUCKET);
        expect(pushSpy).toHaveBeenCalledWith('/files');
    });
});