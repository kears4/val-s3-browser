import { IconButton, Menu, Typography } from '@material-ui/core';
import React from 'react';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import BucketEditMenuProps from './BucketEditMenuProps';
import { useState } from 'react';
import BucketEditMenuState from './BucketEditMenuState';
import { MenuItem } from '@material-ui/core';
import BucketDeleteDialog from './bucketDeleteDialog/BucketDeleteDialog';

export default function BucketEditMenu(props: BucketEditMenuProps) {
    const [state, setState] = useState<BucketEditMenuState>({ anchorElement: null, isBucketDeleteDialogOpen: false });
    const open = Boolean(state.anchorElement);

    const openMenu = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newState = { ...state, ...{ anchorElement: event.currentTarget } };
        setState(newState);
    };

    const handleClose = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newState = { ...state, ...{ anchorElement: null } };
        setState(newState);
    };

    const handleClickDeleteBucket = () => {
        setState({ isBucketDeleteDialogOpen: true, anchorElement: null });
    };

    const handleCloseBucketDeleteDialog = () => {
        setState({ isBucketDeleteDialogOpen: false, anchorElement: null });
    };

    return (
        <>
            <IconButton
                id='bucketEditButton'
                color='inherit'
                onClick={openMenu as any}
                data-test-id='bucketEditButton'>
                <MoreVertIcon />
            </IconButton>

            <Menu
                open={open}
                onClose={handleClose}
                anchorEl={state.anchorElement}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                data-test-id='bucketEditMenu'>

                <MenuItem data-test-id='deleteBucketMenuItem' onClick={handleClickDeleteBucket}>
                    <Typography>
                        Supprimer
                    </Typography>
                </MenuItem>
            </Menu>

            <BucketDeleteDialog isOpen={state.isBucketDeleteDialogOpen} closeDialog={handleCloseBucketDeleteDialog}
                data-test-id='deleteBucketDialog' />
        </>
    );
}