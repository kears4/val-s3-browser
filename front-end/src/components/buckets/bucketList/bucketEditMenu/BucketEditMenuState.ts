export default interface BucketEditMenuState {
    anchorElement: any | null;
    isBucketDeleteDialogOpen: boolean;
}