console.error = jest.fn();

import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { find } from '../../../../tests/utils/FindHelpers';
import BucketEditMenu from './BucketEditMenu';
import BucketEditMenuProps from './BucketEditMenuProps';

describe('<BucketEditMenu />', () => {
    let wrapper: ShallowWrapper<BucketEditMenuProps>;

    beforeEach(() => {
        wrapper = shallow((<BucketEditMenu />));
    });

    it('Has editButton', () => {
        const editButton = find(wrapper, 'bucketEditButton');
        expect(editButton.exists()).toBeTruthy();
    });

    it('by default, edit menu is closed and not anchored', () => {
        const editMenu = find(wrapper, 'bucketEditMenu');

        expect(editMenu.prop('open')).toBeFalsy();
        expect(editMenu.prop('anchorEl')).toBeNull();
    });

    it('has delete bucket menu item', () => {
        const deleteBucketMenuItem = find(wrapper, 'deleteBucketMenuItem');
        expect(deleteBucketMenuItem.exists()).toBeTruthy();
    });

    it('when click on edit button, menu is opened and anchored on edit button', () => {
        // ARRANGE
        const editButton = find(wrapper, 'bucketEditButton');

        // ACT
        editButton.simulate('click', { currentTarget: 'el' });

        // ASSERT
        const editMenu = find(wrapper, 'bucketEditMenu');

        expect(editMenu.prop('open')).toBeTruthy();
        expect(editMenu.prop('anchorEl')).toEqual('el');
    });

    it('when click on menu item, closes menu and opens delete dialog', () => {
        // ARRANGE
        const editButton = find(wrapper, 'bucketEditButton');
        editButton.simulate('click', { currentTarget: 'el' });

        const deleteBucketMenuItem = find(wrapper, 'deleteBucketMenuItem');

        // ACT
        deleteBucketMenuItem.simulate('click');

        // ASSERT
        const editMenu = find(wrapper, 'bucketEditMenu');

        expect(editMenu.prop('open')).toBeFalsy();
        expect(editMenu.prop('anchorEl')).toBeNull();

        const deleteBucketDialog = find(wrapper, 'deleteBucketDialog');
        expect(deleteBucketDialog.prop('open')).toBeFalsy();
    });
});