export default interface BucketDeleteDialogProps {
    isOpen: boolean;
    closeDialog: () => void;
}