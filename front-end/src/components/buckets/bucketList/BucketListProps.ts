import { List } from 'immutable';
import BucketModel from '../../../models/BucketModel';

export default interface BucketListProps {
    buckets: List<BucketModel>;
}