import { shallow } from 'enzyme';
import { ShallowWrapper } from 'enzyme';
import { List } from 'immutable';
import React from 'react';
import BucketModelObjectMother from '../../../tests/objectmothers/models/BucketModelObjectMother';
import { find, findAllForGroup } from '../../../tests/utils/FindHelpers';
import BucketList from './BucketList';
import BucketListProps from './BucketListProps';

describe('<BucketList />', () => {
    const BUCKET_A = BucketModelObjectMother.get('Bucket A');
    const BUCKET_B = BucketModelObjectMother.get('Bucket B');

    const BUCKETS = List([BUCKET_A, BUCKET_B]);

    let wrapper: ShallowWrapper<BucketListProps>;

    beforeEach(() => {
        wrapper = shallow((<BucketList buckets={BUCKETS} />));
    });

    it('has all bucket rows', () => {
        const bucketRowsGroup = findAllForGroup(wrapper, 'bucketRows');
        expect(bucketRowsGroup.length).toEqual(2);
    });

    it('has a bucket for each for', () => {
        const bucketRowA = find(wrapper, 'bucketRow0');
        expect(bucketRowA.prop('bucket')).toBe(BUCKET_A);

        const bucketRowB = find(wrapper, 'bucketRow1');
        expect(bucketRowB.prop('bucket')).toBe(BUCKET_B);
    });
});