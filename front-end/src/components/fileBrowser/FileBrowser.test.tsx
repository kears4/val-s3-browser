import { ShallowWrapper, shallow } from 'enzyme';
import FileBrowser from './FileBrowser';
import React from 'react';
import { find } from '../../tests/utils/FindHelpers';

describe('<FileBrowser />', () => {

    let wrapper: ShallowWrapper<FileBrowser>;

    beforeEach(() => {
        wrapper = shallow(<FileBrowser />);
    });

    it('has FileBrowserMenu component', () => {
        const downloadIcon = find(wrapper, 'fileBrowserMenu');
        expect(downloadIcon.exists()).toBeTruthy();
    });

    it('has FolderStructure component', () => {
        const downloadIcon = find(wrapper, 'folderStructure');
        expect(downloadIcon.exists()).toBeTruthy();
    });
});