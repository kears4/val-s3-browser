import { ShallowWrapper, shallow, ReactWrapper } from 'enzyme';
import BucketBrowser from './BucketBrowser';
import React from 'react';
import { BucketBrowserProps } from './BucketBrowserProps';
import { find } from '../../../tests/utils/FindHelpers';
import BucketModelObjectMother from '../../../tests/objectmothers/models/BucketModelObjectMother';
import { List } from 'immutable';

describe('<BucketBrowser />', () => {
    const BUCKET_A = BucketModelObjectMother.get('bucketA');
    const BUCKET_B = BucketModelObjectMother.get('bucketB');

    let wrapper: ReactWrapper<BucketBrowserProps> | ShallowWrapper<BucketBrowserProps>;
    let openBucket: jest.Mock;

    beforeEach(() => {
        openBucket = jest.fn();
    });

    describe('with buckets', () => {
        beforeEach(() => {
            wrapper = shallow(<BucketBrowser buckets={List([BUCKET_A, BUCKET_B])} openBucket={openBucket} />);
        });

        it('has buckets as options', () => {
            const comboBox = find(wrapper, 'bucketsComboBox');
            expect(comboBox.prop('items')).toEqual([
                { value: BUCKET_A, text: BUCKET_A.name },
                { value: BUCKET_B, text: BUCKET_B.name }
            ]);
        });
    });

    describe('with selected bucket', () => {
        beforeEach(() => {
            wrapper = shallow(<BucketBrowser buckets={List([BUCKET_A, BUCKET_B])} currentBucket={BUCKET_B}
                openBucket={openBucket} />);
        });

        it('has buckets as selected option', () => {
            const comboBox = find(wrapper, 'bucketsComboBox');
            expect(comboBox.prop('selectedValue')).toEqual(BUCKET_B);
        });
    });

    describe('can select bucket', () => {
        beforeEach(() => {
            wrapper = shallow(<BucketBrowser buckets={List([BUCKET_A, BUCKET_B])} openBucket={openBucket} />);
        });

        it('has buckets as selected option', () => {
            const comboBox = find(wrapper, 'bucketsComboBox');
            comboBox.simulate('change', BUCKET_B);

            expect(openBucket).toHaveBeenCalledWith(BUCKET_B);
        });
    });
});