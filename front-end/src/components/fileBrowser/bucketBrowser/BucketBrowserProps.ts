import { List } from 'immutable';
import BucketModel from '../../../models/BucketModel';

export interface BucketBrowserMapToStateProps {
    buckets: List<BucketModel>;
    currentBucket?: BucketModel;
}

export interface BucketBrowserMapToDispatch {
    openBucket: (bucket: BucketModel) => void;
}

export type BucketBrowserProps = BucketBrowserMapToStateProps & BucketBrowserMapToDispatch;