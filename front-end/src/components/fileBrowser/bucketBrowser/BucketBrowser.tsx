import React from 'react';

import { BucketBrowserProps } from './BucketBrowserProps';
import ComboBox from '../../common/comboBox/ComboBox';
import BucketModel from '../../../models/BucketModel';
import { Box, CardContent } from '@material-ui/core';
import { Card } from '@material-ui/core';

export default class BucketBrowser extends React.Component<BucketBrowserProps> {
    render() {
        const comboBoxItems = this.props.buckets.map((b) => {
            return {
                value: b,
                text: b.name
            };
        }).toArray();

        return (
            <Box sx={{ mt: 3 }}>
                <Card>
                    <CardContent>
                        <Box sx={{ maxWidth: 500 }}>
                            <ComboBox
                                items={comboBoxItems}
                                selectedValue={this.props.currentBucket}
                                onChange={(value: BucketModel) => this.props.openBucket(value)}
                                placeholder='Sélectionner un compartiment'
                                data-test-id='bucketsComboBox' />
                        </Box>
                    </CardContent>
                </Card>
            </Box>
        );
    }
}