import { Box } from '@material-ui/core';
import React from 'react';
import UploadButtonContainer from '../../../containers/upload/UploadButtonContainer';

export default function FileToolbar() {
    return (
        <>
            <Box >
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'flex-end'
                    }}>

                    <UploadButtonContainer data-test-id='uploadFileButton' />
                </Box>
            </Box>
        </>
    );
}