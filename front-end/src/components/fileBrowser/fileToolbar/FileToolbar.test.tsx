import React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import { find } from '../../../tests/utils/FindHelpers';
import FileToolbar from './FileToolbar';


describe('<FileToolbar />', () => {
    let wrapper: ShallowWrapper<AnalyserNode, any, any>;

    beforeEach(() => {
        wrapper = shallow(<FileToolbar />);
    });

    it('has uploadFileButton', () => {
        const uploadFileButton = find(wrapper, 'uploadFileButton');
        expect(uploadFileButton.exists()).toBeTruthy();
    });
});
