import { ShallowWrapper, shallow } from 'enzyme';
import Folder from './Folder';
import React from 'react';
import { find } from '../../../tests/utils/FindHelpers';
import FolderModelObjectMother from '../../../tests/objectmothers/models/FolderModelObjectMother';
import { FolderProps } from './FolderProps';

describe('<Folder />', () => {
    const FOLDER = FolderModelObjectMother.get();
    let onBrowseClick: jest.Mock;

    let wrapper: ShallowWrapper<FolderProps>;

    beforeEach(() => {
        onBrowseClick = jest.fn();
        wrapper = shallow(<Folder folder={FOLDER} browse={onBrowseClick} />);
    });

    it('renders name', () => {
        expect(wrapper.text()).toMatch(FOLDER.name);
    });

    it('has browse icon', () => {
        const browseIcon = find(wrapper, 'browseIcon');
        expect(browseIcon.exists()).toBeTruthy();
    });

    it('can browse', () => {
        const browseButton = find(wrapper, 'browseButton');
        browseButton.simulate('click');

        expect(onBrowseClick).toHaveBeenCalled();
    });
});