import React from 'react';

import FolderIcon from '@material-ui/icons/Folder';
import { FolderProps } from './FolderProps';
import { Button, Typography } from '@material-ui/core';


export default class Folder extends React.Component<FolderProps> {
    render() {
        return (
            <>
                <Button data-test-id='browseButton' onClick={() => this.props.browse(this.props.folder)}
                    color='inherit'>
                    <FolderIcon data-test-id='browseIcon' />
                </Button>

                <Typography data-test-id={`folder-${this.props.folder.name}`}>
                    {this.props.folder.name}
                </Typography>
            </>
        );
    }
}