import FolderModel from '../../../models/FolderModel';

export interface FolderMapToStateProps {
    folder: FolderModel;
}

export interface FolderMapToDispatch {
    browse: (folder: FolderModel) => void;
}

export type FolderProps = FolderMapToStateProps & FolderMapToDispatch;