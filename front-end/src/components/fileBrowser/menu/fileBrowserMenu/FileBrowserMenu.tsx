import React from 'react';

import FileBrowserMenuProps from './FileBrowserMenuProps';
import BrowseToParentContainer from '../../../../containers/fileBrowser/menu/browseToParent/BrowseToParentContainer';
import PathContainer from '../../../../containers/fileBrowser/menu/path/PathContainer';
import { Box, Paper } from '@material-ui/core';

export default class FileBrowserMenu extends React.Component<FileBrowserMenuProps> {
    render() {
        return (
            <Box sx={{ p: 2 }}>
                <Paper elevation={9}>
                    <BrowseToParentContainer data-test-id='browseToParent' />
                    <PathContainer data-test-id='path' />
                </Paper>
            </Box>
        );
    }
}