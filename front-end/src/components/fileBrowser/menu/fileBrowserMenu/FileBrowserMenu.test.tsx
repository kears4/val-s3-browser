import { ShallowWrapper, shallow } from 'enzyme';
import FileBrowserMenu from './FileBrowserMenu';
import React from 'react';
import { find } from '../../../../tests/utils/FindHelpers';

describe('<FileBrowserMenu />', () => {

    let wrapper: ShallowWrapper<FileBrowserMenu>;

    beforeEach(() => {
        wrapper = shallow(<FileBrowserMenu />);
    });

    it('has BrowseToParent component', () => {
        const downloadIcon = find(wrapper, 'browseToParent');
        expect(downloadIcon.exists()).toBeTruthy();
    });

    it('has Path component', () => {
        const downloadIcon = find(wrapper, 'path');
        expect(downloadIcon.exists()).toBeTruthy();
    });
});