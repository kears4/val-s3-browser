
// tslint:disable-next-line:no-empty-interface
export interface BrowseToParentMapToStateProps {
    disabled: boolean;
}

export interface BrowseToParentMapToDispatch {
    browseToParent: () => void;
}

export type BrowseToParentProps = BrowseToParentMapToStateProps & BrowseToParentMapToDispatch;