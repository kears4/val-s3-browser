import React from 'react';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';

import { BrowseToParentProps } from './BrowseToParentProps';
import { Button } from '@material-ui/core';

export default class BrowseToParent extends React.Component<BrowseToParentProps> {
    render() {
        return (
            <Button data-test-id='browseToParentButton'
                onClick={() => this.props.browseToParent()}
                disabled={this.props.disabled}
                color='primary'
                variant='contained'
                sx={{ margin: '10px 0px 10px 10px' }}>

                <KeyboardReturnIcon />
            </Button>
        );
    }
}