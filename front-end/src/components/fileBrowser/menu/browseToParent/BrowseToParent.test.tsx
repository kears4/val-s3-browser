import { ShallowWrapper, shallow } from 'enzyme';
import BrowseToParent from './BrowseToParent';
import React from 'react';
import { find } from '../../../../tests/utils/FindHelpers';
import { BrowseToParentProps } from './BrowseToParentProps';

describe('<BrowseToParent />', () => {
    let onBrowseClick: jest.Mock;

    let wrapper: ShallowWrapper<BrowseToParentProps>;

    beforeEach(() => {
        onBrowseClick = jest.fn();
        wrapper = shallow(<BrowseToParent browseToParent={onBrowseClick} disabled={false} />);
    });

    it('renders return button', () => {
        expect(find(wrapper, 'browseToParentButton').exists()).toBeTruthy();
    });

    it('can browse', () => {
        const browsetoParentButton = find(wrapper, 'browseToParentButton');
        browsetoParentButton.simulate('click');

        expect(onBrowseClick).toHaveBeenCalled();
    });

    it('can be disabled', () => {
        wrapper = shallow(<BrowseToParent browseToParent={onBrowseClick} disabled={true} />);
        const browsetoParentButton = find(wrapper, 'browseToParentButton');
        expect(browsetoParentButton.prop('disabled')).toBeTruthy();
    });
});