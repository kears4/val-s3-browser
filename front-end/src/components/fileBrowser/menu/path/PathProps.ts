import { List } from 'immutable';
import FolderModel from '../../../../models/FolderModel';

export interface PathMapToStateProps {
    folderPath: List<FolderModel>;
}

// tslint:disable-next-line:no-empty-interface
export interface PathMapToDispatch {
}

export type PathProps = PathMapToStateProps & PathMapToDispatch;