import { ShallowWrapper, shallow } from 'enzyme';
import Path from './Path';
import React from 'react';
import FolderModelObjectMother from '../../../../tests/objectmothers/models/FolderModelObjectMother';
import { PathProps } from './PathProps';
import { List } from 'immutable';

describe('<Path />', () => {
    const FOLDER_A = FolderModelObjectMother.get('folderA');
    const FOLDER_B = FolderModelObjectMother.get('folderB');

    let onDownloadClick: jest.Mock;

    let wrapper: ShallowWrapper<PathProps>;

    describe('With one folder in path', () => {
        beforeEach(() => {
            onDownloadClick = jest.fn();
            wrapper = shallow(<Path folderPath={List([FOLDER_A])} />);
        });

        it('renders path with folder name', () => {
            expect(wrapper.text()).toMatch('\\folderA');
        });
    });

    describe('With two folders in path', () => {
        beforeEach(() => {
            onDownloadClick = jest.fn();
            wrapper = shallow(<Path folderPath={List([FOLDER_A, FOLDER_B])} />);
        });

        it('renders path with both folders name', () => {
            expect(wrapper.text()).toMatch('\\folderA\\folderB');
        });
    });
});