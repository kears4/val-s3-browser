import { Typography } from '@material-ui/core';
import React from 'react';

import { PathProps } from './PathProps';

export default class Path extends React.Component<PathProps> {
    render() {
        const pathText = this.props.folderPath.map(f => '\\' + f.name).join('');

        return (
            <Typography display='inline' data-test-id='folderPath'>
                {pathText}
            </Typography>
        );
    }
}