import { Paper } from '@material-ui/core';
import React from 'react';
import FolderStructureContainer from '../../containers/fileBrowser/folderStructure/FolderStructureContainer';

import FileBrowserProps from './FileBrowserProps';
import FileBrowserMenu from './menu/fileBrowserMenu/FileBrowserMenu';

export default class FileBrowser extends React.Component<FileBrowserProps> {
    render() {
        return (
            <Paper elevation={0}>
                <FileBrowserMenu data-test-id='fileBrowserMenu' />
                <FolderStructureContainer data-test-id='folderStructure' />
            </Paper>
        );
    }
}