import React from 'react';

import GetAppIcon from '@material-ui/icons/GetApp';
import { FileProps } from './FileProps';
import FileServices from '../../../services/file/FileServices';
import BucketModel from '../../../models/BucketModel';
import FileModel from '../../../models/FileModel';
import WindowHelper from '../../../helpers/DownloadHelper';
import { Typography } from '@material-ui/core';
import { Button } from '@material-ui/core';

export default class File extends React.Component<FileProps> {
    async downloadFile(bucket: BucketModel, file: FileModel) {
        const downloadUrl = await FileServices.getDownloadUrl(bucket, file);
        WindowHelper.downloadUrl(downloadUrl);
    }

    render() {
        const file = this.props.file;
        const bucket = this.props.bucket ?? new BucketModel('');

        return (
            <>
                <Button data-test-id='downloadButton' onClick={() => this.downloadFile(bucket, file)} color='inherit'>
                    <GetAppIcon data-test-id='downloadIcon' />
                </Button>

                <Typography data-test-id={`file-${this.props.file.name}`} >
                    {this.props.file.name}
                </Typography>
            </>
        );
    }
}