import { ShallowWrapper, shallow } from 'enzyme';
import File from './File';
import React from 'react';
import FileModelObjectMother from '../../../tests/objectmothers/models/FileModelObjectMother';
import { find } from '../../../tests/utils/FindHelpers';
import { FileProps } from './FileProps';
import BucketModelObjectMother from '../../../tests/objectmothers/models/BucketModelObjectMother';
import FileServices from '../../../services/file/FileServices';
import { when } from 'jest-when';
import DownloadHelper from '../../../helpers/DownloadHelper';

describe('<File />', () => {
    const FILE = FileModelObjectMother.get();
    const BUCKET = BucketModelObjectMother.get();

    let wrapper: ShallowWrapper<FileProps>;

    beforeEach(() => {
        wrapper = shallow(<File file={FILE} bucket={BUCKET} />);
        FileServices.getDownloadUrl = jest.fn();
        DownloadHelper.downloadUrl = jest.fn();
    });

    it('renders name', () => {
        expect(wrapper.text()).toMatch(FILE.name);
    });

    it('has download icon', () => {
        const downloadIcon = find(wrapper, 'downloadIcon');
        expect(downloadIcon.exists()).toBeTruthy();
    });

    it('can download from download button', async () => {
        // ARRANGE
        const downloadButton = find(wrapper, 'downloadButton');

        when(FileServices.getDownloadUrl as any)
            .calledWith(BUCKET, FILE)
            .mockReturnValue('www.download.com');

        // ACT
        await downloadButton.simulate('click');

        // ASSERT
        expect(DownloadHelper.downloadUrl).toHaveBeenCalledWith('www.download.com');
    });
});