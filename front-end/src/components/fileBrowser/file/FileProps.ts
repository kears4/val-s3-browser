import BucketModel from '../../../models/BucketModel';
import FileModel from '../../../models/FileModel';

export interface FileContainerProps {
    file: FileModel;
}
export interface FileMapToStateProps {
    bucket?: BucketModel;
}



export type FileProps = FileMapToStateProps & FileContainerProps;