console.error = jest.fn();

import { ShallowWrapper, shallow, ReactWrapper } from 'enzyme';
import FolderStructure from './FolderStructure';
import React from 'react';
import FolderProps from './FolderStructureProps';
import { find, findAllForGroup } from '../../../tests/utils/FindHelpers';
import FolderModelObjectMother from '../../../tests/objectmothers/models/FolderModelObjectMother';
import FileModelObjectMother from '../../../tests/objectmothers/models/FileModelObjectMother';

describe('<FolderStructure />', () => {
    const FILE = FileModelObjectMother.get();
    const FOLDER = FolderModelObjectMother.get();

    const CONTAINING_FOLDER = FolderModelObjectMother.getWithContent([FILE, FOLDER]);

    let wrapper: ReactWrapper<FolderProps> | ShallowWrapper<FolderProps>;

    beforeEach(() => {
        wrapper = shallow(<FolderStructure folder={CONTAINING_FOLDER} />);
    });

    it('with no folder returns ...', () => {
        wrapper = shallow(<FolderStructure folder={undefined} />);
        expect(wrapper.text()).toContain('...');
    });

    it('renders all items in containing folder', () => {
        wrapper = shallow(<FolderStructure folder={CONTAINING_FOLDER} />);

        const folderItems = findAllForGroup(wrapper, 'folder-item');
        expect(folderItems.length).toEqual(2);
    });

    it('renders file item', () => {
        const fileId = `file-container-${FILE.name}`;
        const file = find(wrapper, fileId);

        expect(file.exists()).toBeTruthy();
    });

    it('renders folder item', () => {
        const folderId = `folder-container-${FOLDER.name}`;
        const folder = find(wrapper, folderId);

        expect(folder.exists()).toBeTruthy();
    });
});