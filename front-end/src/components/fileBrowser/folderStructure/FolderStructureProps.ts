import FolderModel from '../../../models/FolderModel';

export default interface FolderStructureProps {
    folder: FolderModel | undefined;
}