import React from 'react';

import FolderStructureProps from './FolderStructureProps';
import { Divider, List, ListItem } from '@material-ui/core';
import FolderOrFileFactory from './FolderOrFileFactory';
import { Typography } from '@material-ui/core';

const noContentStyle = {
    textAlign: 'center',
    fontSize: 'large'
};

export default class FolderStructure extends React.Component<FolderStructureProps> {
    render() {
        if (!this.props.folder) {
            return (
                <Typography style={noContentStyle as any}>
                    ...
                </Typography>);
        }

        const folderItems = this.props.folder.content.map((fi) => {
            return (
                <>
                    <ListItem key={fi.name} data-test-group='folder-item' >
                        {FolderOrFileFactory.getFileOrFolder(fi)}
                    </ListItem>
                    <Divider />
                </>
            );
        });

        return (
            <List>
                {folderItems}
            </List>
        );
    }
}