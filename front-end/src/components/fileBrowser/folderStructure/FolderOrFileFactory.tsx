import React from 'react';


import FileModel from '../../../models/FileModel';
import FolderModel from '../../../models/FolderModel';
import FileContainer from '../../../containers/fileBrowser/file/FileContainer';
import FolderContainer from '../../../containers/fileBrowser/folder/FolderContainer';

function getFileOrFolder(item: FileModel | FolderModel) {
    if (item instanceof FileModel) {
        return (
            <FileContainer file={item} data-test-id={`file-container-${item.name}`} />
        );
    }

    return (
        <FolderContainer folder={item} data-test-id={`folder-container-${item.name}`} />
    );
}


export default {
    getFileOrFolder
};