import { mount, shallow } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import store from '../../../Store';
import FileModelObjectMother from '../../../tests/objectmothers/models/FileModelObjectMother';
import FolderModelObjectMother from '../../../tests/objectmothers/models/FolderModelObjectMother';
import { find } from '../../../tests/utils/FindHelpers';
import FolderOrFileFactory from './FolderOrFileFactory';

describe('FolderOrFileFactory', () => {
    const FILE = FileModelObjectMother.get();
    const FOLDER = FolderModelObjectMother.get();


    it('returns file for FileModel', () => {
        const wrapper = shallow(<Provider store={store}>
            {FolderOrFileFactory.getFileOrFolder(FILE)}
        </Provider>);

        const fileId = `file-container-${FILE.name}`;
        const file = find(wrapper, fileId);

        expect(file.exists()).toBeTruthy();
        expect(file.prop('file')).toBe(FILE);
    });

    it('returns folder for FolderModel', () => {
        const wrapper = shallow(<Provider store={store}>
            {FolderOrFileFactory.getFileOrFolder(FOLDER)}
        </Provider>);

        const folderId = `folder-container-${FOLDER.name}`;
        const folder = find(wrapper, folderId);

        expect(folder.exists()).toBeTruthy();
        expect(folder.prop('folder')).toBe(FOLDER);
    });
});