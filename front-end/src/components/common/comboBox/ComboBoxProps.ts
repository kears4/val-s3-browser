export default interface ComboBoxProps {
    items: { value: any, text: any }[];
    selectedValue?: any;
    onChange: (value: any) => void;
    disabled?: boolean;
    placeholder?: string;
}
