import { shallow } from 'enzyme';
import * as React from 'react';
import { find, findAllForGroup } from '../../../tests/utils/FindHelpers';
import ComboBox from './ComboBox';


describe('<ComboBox />', () => {
    const values = [
        { value: 0, text: 'A' },
        { value: 1, text: 'B' },
        { value: 2, text: 'C' }];

    let selectedValue: number | undefined;
    const valueSelector = (value: number) => {
        selectedValue = value;
    };

    beforeEach(() => {
        selectedValue = undefined;
    });

    it('With data, option values are from data', () => {
        // ARRANGE
        const wrapper = shallow(<ComboBox items={values} onChange={valueSelector} />);

        // ACT
        const options = findAllForGroup(wrapper, 'options');

        // ASSERT
        expect(options.length).toBe(3);

        expect(options.get(0).props.children).toBe(values[0].text);
        expect(options.get(1).props.children).toBe(values[1].text);
        expect(options.get(2).props.children).toBe(values[2].text);
    });

    it('With no initial value, value is not in select', () => {
        // ARRANGE
        const wrapper = shallow(<ComboBox items={values} onChange={valueSelector} />);

        // ACT
        const select = find(wrapper, 'comboBox');

        // ASSERT
        expect(select.prop('value')).toBe(undefined);
    });

    it('Placeholder sets a special menu item not selectable', () => {
        // ARRANGE
        const wrapper = shallow(<ComboBox items={values} onChange={valueSelector} placeholder={'placeholder'} />);

        // ACT
        const placeHolderOption = find(wrapper, 'option-1');

        // ASSERT
        expect(placeHolderOption.prop('value')).toBe('none');
        expect(placeHolderOption.text()).toBe('placeholder');
        expect(placeHolderOption.prop('disabled')).toBeTruthy();
    });

    it('With no place holder, item is not added', () => {
        // ARRANGE
        const wrapper = shallow(<ComboBox items={[]} onChange={valueSelector} />);

        // ACT
        const placeHolderOption = find(wrapper, 'option-1');

        // ASSERT
        expect(placeHolderOption.exists()).toBeFalsy();
    });

    it('With initial value, value is in select', () => {
        // ARRANGE
        const wrapper = shallow(<ComboBox items={values} onChange={valueSelector} selectedValue={1} />);

        // ACT
        const select = find(wrapper, 'comboBox');

        // ASSERT
        expect(select.prop('value')).toBe(1); // 1 is B
    });

    it('With data, select last option, last option is selected and passed to onChangeHandler', () => {
        // ARRANGE
        const wrapper = shallow(<ComboBox items={values} onChange={valueSelector} />);
        const select = find(wrapper, 'comboBox');

        // ACT
        select.simulate('change', { target: { value: values[2].value } });

        // ASSERT
        expect(selectedValue).toEqual(values[2].value);
    });

    it('With data and disabled, combobox is disabled.', () => {
        // ARRANGE
        const wrapper = shallow(<ComboBox items={values} onChange={valueSelector} disabled={true} />);

        // ACT
        const comboBox = find(wrapper, 'comboBox');

        // ASSERT
        expect(comboBox.prop('disabled')).toBeTruthy();
    });
});
