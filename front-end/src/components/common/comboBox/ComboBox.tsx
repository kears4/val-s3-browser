import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import * as React from 'react';

import ComboBoxProps from './ComboBoxProps';

export default class ComboBox extends React.Component<ComboBoxProps> {
    public handleChange(event: React.ChangeEvent<HTMLSelectElement>) {
        this.props.onChange(event.target.value);
    }

    public render() {
        const options = this.props.items.map((item, index) => {
            return (<MenuItem
                key={index}
                value={item.value}
                data-test-group='options'
                data-test-id={`option${index}`}>
                {item.text}
            </MenuItem>);
        });

        const placeHolderItem = (<MenuItem
            key={-1}
            value={'none'}
            disabled
            data-test-group='options'
            data-test-id='option-1'>
            {this.props.placeholder}
        </MenuItem>);

        const fullOptions = !this.props.placeholder
            ? options
            : [placeHolderItem, ...options];

        return (
            <Select
                defaultValue='none'
                value={this.props.selectedValue}
                onChange={(event) => {
                    this.handleChange(event as any);
                }}
                disabled={this.props.disabled}
                variant='outlined'
                data-test-id='comboBox'
            >
                {fullOptions}
            </Select>
        );
    }
}
