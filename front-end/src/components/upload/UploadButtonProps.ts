import BucketModel from '../../models/BucketModel';
import FoldersState from '../../reducers/folders/FoldersState';

export interface UploadButtonMapToStateProps {
    folderState: FoldersState;
    bucket?: BucketModel;
    disabled: boolean;
}

export interface UploadButtonMapToDispatchProps {
    uploadFile: (folderState: FoldersState, bucket: BucketModel, file: File) => void;
}

export type UploadButtonProps = UploadButtonMapToStateProps & UploadButtonMapToDispatchProps;