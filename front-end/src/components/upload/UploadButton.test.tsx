import { ShallowWrapper, shallow } from 'enzyme';
import React from 'react';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import FoldersStateObjectMother from '../../tests/objectmothers/state/FoldersStateObjectMother';
import { UploadButtonProps } from './UploadButtonProps';
import UploadButton from './UploadButton';
import { find } from '../../tests/utils/FindHelpers';

describe('<UploadButton />', () => {
    const BUCKET = BucketModelObjectMother.get();
    const FOLDER_STATE = FoldersStateObjectMother.get();
    const FILE: File = { name: 'File.mid' } as any;

    let uploadFileMock: jest.Mock;

    let wrapper: ShallowWrapper<UploadButtonProps>;

    beforeEach(() => {
        uploadFileMock = jest.fn();
        wrapper = shallow(<UploadButton bucket={BUCKET} folderState={FOLDER_STATE} uploadFile={uploadFileMock}
            disabled={false} />);

    });

    it('when disabled, upload button is disabled', () => {
        wrapper = shallow(<UploadButton bucket={BUCKET} folderState={FOLDER_STATE} uploadFile={uploadFileMock}
            disabled={true} />);
        const uploadButton = find(wrapper, 'uploadButton');
        expect(uploadButton.prop('disabled')).toBeTruthy();
    });

    it('has upload icon', () => {
        const uploadIcon = find(wrapper, 'uploadIcon');
        expect(uploadIcon.exists()).toBeTruthy();
    });

    it('has upload label', () => {
        const uploadButton = find(wrapper, 'uploadButton');
        expect(uploadButton.text()).toEqual('Téléverser');
    });

    it('can upload file from upload input', async () => {
        // ARRANGE
        const uploadButton = find(wrapper, 'uploadInput');
        const event = { target: { files: [FILE] } };

        // ACT
        await uploadButton.simulate('change', event);

        // ASSERT
        expect(uploadFileMock).toHaveBeenCalledWith(FOLDER_STATE, BUCKET, FILE);
    });
});