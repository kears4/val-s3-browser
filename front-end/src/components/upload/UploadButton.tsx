import React from 'react';

import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import BucketModel from '../../models/BucketModel';
import { UploadButtonProps } from './UploadButtonProps';
import { Button, Typography } from '@material-ui/core';

export default class UploadButton extends React.Component<UploadButtonProps> {
    onChangeHandler(event: any) {
        const file = event.target.files[0];
        const bucket = this.props.bucket ?? new BucketModel('');

        this.props.uploadFile(this.props.folderState, bucket, file);
    }

    uploadFile() {
        const uploadButton = document.getElementById('uploadInput');
        if (!uploadButton) throw new Error('Upload input inexistent');

        const currentGatherFilesAndFireEvent = (e: any) => {
            this.onChangeHandler(e);
        };

        uploadButton.addEventListener('change', currentGatherFilesAndFireEvent, false);
        uploadButton.click();
        uploadButton.removeEventListener('change', currentGatherFilesAndFireEvent, false);
        (uploadButton as HTMLButtonElement).value = '';

    }

    render() {
        return (<>
            <div style={{ width: '0px', height: '0px', overflow: 'hidden' }}>
                <input
                    id='uploadInput'
                    type='file'
                    name='file'
                    onChange={(event: any) => this.onChangeHandler(event)}
                    data-test-id='uploadInput' />
            </div>

            <Button
                disabled={this.props.disabled}
                color='primary'
                variant='contained'
                data-test-id='uploadButton'
            >
                <CloudUploadIcon data-test-id='uploadIcon' />
                <Typography sx={{ marginLeft: '10px' }} variant='button' onClick={() => this.uploadFile()}>
                    Téléverser
                </Typography>
            </Button>
        </>);
    }
}