import React from 'react';

import {
    NavLink as RouterLink,
    useLocation
} from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button, ListItem } from '@material-ui/core';

const buttonStyle = (active: boolean) => {
    const buttonNotActiveStyle = {
        color: 'secondary.main',
        fontWeight: 'medium',
        justifyContent: 'flex-start',
        letterSpacing: 0,
        py: 1.25,
        textTransform: 'none',
        width: '100%',
        '& svg': {
            mr: 1
        }
    };

    if (!active) return buttonNotActiveStyle;

    return {
        ...buttonNotActiveStyle, ...{
            color: 'secondary.contrastText',
            fontWeight: 'bold',
        }
    };
};

const NavItem = ({
    href,
    icon: Icon,
    title,
}: { href: string, icon: any, title: string }) => {
    const location = useLocation();

    const active = href === location.pathname;

    return (
        <ListItem
            disableGutters
            sx={{
                display: 'flex',
                py: 0
            }}
        >
            <Button
                component={RouterLink}
                sx={buttonStyle(active) as any}
                to={href}
            >
                {Icon && (
                    <Icon size='20' />
                )}
                <span>
                    {title}
                </span>
            </Button>
        </ListItem>
    );
};

NavItem.propTypes = {
    href: PropTypes.string,
    icon: PropTypes.elementType,
    title: PropTypes.string
};

export default NavItem;
