import React from 'react';

import {
    Box,
    Drawer,
    List,
} from '@material-ui/core';
import {
    Folder as FolderIcon,
    File as FileIcon
} from 'react-feather';
import NavItem from './NavItem';
import ProfileContainer from '../../containers/profile/ProfileContainer';

const items = [
    {
        href: '/buckets',
        icon: FolderIcon,
        title: 'Compartiments'
    },
    {
        href: '/files',
        icon: FileIcon,
        title: 'Fichiers'
    }
];

const NavigationMenu = () => {
    const content = (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                height: '100%'
            }}
        >
            <ProfileContainer />
            <Box sx={{ p: 2 } as any}>
                <List>
                    {items.map((item) => (
                        <NavItem
                            href={item.href}
                            key={item.title}
                            title={item.title}
                            icon={item.icon}
                        />
                    ))}
                </List>
            </Box>
        </Box>
    );

    return (
        <>
            <Drawer
                anchor='left'
                variant='persistent'
                open={true}
                PaperProps={{
                    style: {
                        width: '256px',
                        top: '64px',
                        height: 'calc(100% - 64px)'
                    }
                } as any}
            >
                {content}
            </Drawer>
        </>
    );
};

export default NavigationMenu;
