import React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import MainLayout from './MainLayout';
import { find } from '../../tests/utils/FindHelpers';


describe('<MainLayout />', () => {
    let wrapper: ShallowWrapper<AnalyserNode, any, any>;
    let onLogout: jest.Mock;

    beforeEach(() => {
        onLogout = jest.fn();

        wrapper = shallow(<MainLayout logout={onLogout} />);
    });

    it('has title menu with logout prop drilled downed', () => {
        const titleMenu = find(wrapper, 'titleMenu');
        expect(titleMenu.exists()).toBeTruthy();
        expect(titleMenu.prop('logout')).toBe(onLogout);
    });

    it('has navigation menu', () => {
        const titleMenu = find(wrapper, 'navigationMenu');
        expect(titleMenu.exists()).toBeTruthy();
    });
});
