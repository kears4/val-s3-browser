import React from 'react';
import NavigationMenu from '../navigation/NavigationMenu';
import TitleMenu from '../titleMenu/TitleMenu';
import MainLayoutProps from './MainLayoutProps';

const layoutStyle = {
    display: 'flex',
    overflow: 'hidden',
    width: '100%'
};

export default class MainLayout extends React.Component<MainLayoutProps> {
    render() {
        return (<div style={layoutStyle}>
            <TitleMenu data-test-id='titleMenu' logout={this.props.logout} />
            <NavigationMenu data-test-id='navigationMenu' />
        </div>);
    }
}