export default interface MainLayoutProps {
    logout: () => void;
}