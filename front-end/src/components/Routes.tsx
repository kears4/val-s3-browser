import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import BucketBrowserPage from '../pages/bucketBrowserPage/BucketBrowserPage';
import FileBrowserPage from '../pages/fileBrowserPage/FileBrowserPage';

export default class Routes extends React.Component {
	render() {
		return (
			<>
				<Switch>
					<Route path='/buckets' >
						<BucketBrowserPage />
					</Route>

					<Route path='/files' >
						<FileBrowserPage />
					</Route>

					<Route path='/' >
						<Redirect to='/buckets' />
					</Route>
				</Switch>
			</>
		);
	}
}