import { ShallowWrapper, shallow } from 'enzyme';
import React from 'react';
import { find } from '../../tests/utils/FindHelpers';
import TitleMenu from './TitleMenu';
import { TitleMenuProps } from './TitleMenuProps';

describe('<TitleMenu />', () => {
    let wrapper: ShallowWrapper<TitleMenuProps>;

    let onLogout: jest.Mock;

    beforeEach(() => {
        onLogout = jest.fn();

        wrapper = shallow(<TitleMenu logout={onLogout} />);
    });

    it('renders app bar', () => {
        const appBar = find(wrapper, 'appBar');
        expect(appBar.exists()).toBeTruthy();
    });

    it('renders home button', () => {
        const homeButton = find(wrapper, 'homeButton');
        expect(homeButton.exists()).toBeTruthy();
    });

    it('can logout', () => {
        const logoutButton = find(wrapper, 'logoutButton');
        logoutButton.simulate('click');

        expect(onLogout).toHaveBeenCalled();
    });
});