import { AppBar, IconButton, Toolbar } from '@material-ui/core';
import React from 'react';
import { TitleMenuProps } from './TitleMenuProps';
import InputIcon from '@material-ui/icons/Input';
import HomeIcon from '@material-ui/icons/Home';
import {
    NavLink as RouterLink,
} from 'react-router-dom';

const appBarStyle = {
    display: 'flex',
    minHeight: '64px'
};

const homeButtonStyle = {
    justifyContent: 'flex-start',
    letterSpacing: 0,
    py: 1.25,
    textTransform: 'none',
    '& svg': {
        mr: 1
    }
};

export default class TitleMenu extends React.Component<TitleMenuProps> {
    render() {
        return (
            <AppBar position='fixed' elevation={0} style={appBarStyle} data-test-id='appBar'>
                <Toolbar>
                    <IconButton color='inherit' component={RouterLink} to='/' sx={homeButtonStyle as any} data-test-id='homeButton'>
                        <HomeIcon fontSize='large' />
                    </IconButton>

                    {/* Use empty div to push content to the right */}
                    <div></div>

                    <IconButton color='inherit' onClick={() => this.props.logout()} data-test-id='logoutButton'>
                        <InputIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
        );
    }
}