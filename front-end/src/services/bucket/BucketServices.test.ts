import { when } from 'jest-when';
import BucketApi from '../../api/BucketApi';
import HttpStatusCodes from '../../helpers/HttpStatusCodes';
import BucketErrors from '../../models/BucketErrors';
import BucketDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/BucketDtoObjectMother';
import BucketServices from './BucketServices';

describe('BucketServices tests', () => {
    const BUCKET_DTO_A = BucketDtoObjectMother.get('bucketA');
    const BUCKET_DTO_B = BucketDtoObjectMother.get('bucketB');

    describe('getBuckets', () => {
        it('gets Buckets from api and maps them', async () => {
            // ARRANGE
            BucketApi.getBuckets = jest.fn();

            when(BucketApi.getBuckets as any)
                .calledWith()
                .mockReturnValue(Promise.resolve([BUCKET_DTO_A, BUCKET_DTO_B]));

            // ACT
            const bucketModels = await BucketServices.getBuckets();

            // ASSERT
            expect(bucketModels).toEqual([
                { name: BUCKET_DTO_A.name },
                { name: BUCKET_DTO_B.name }
            ]);
        });
    });

    describe('createBucket', () => {
        it('when create bucket api is in error, return bucket exists error', async () => {
            // ARRANGE
            BucketApi.createBucket = jest.fn();

            when(BucketApi.createBucket as any)
                .calledWith('new-bucket')
                .mockReturnValue(Promise.resolve(HttpStatusCodes.SERVER_ERROR));

            // ACT
            const errorMessage = await BucketServices.createBucket('new-bucket');

            // ASSERT
            expect(errorMessage).toEqual(BucketErrors.BUCKET_ALREADY_EXISTS);
        });

        it('when create bucket api success, returns undefined', async () => {
            // ARRANGE
            BucketApi.createBucket = jest.fn();

            when(BucketApi.createBucket as any)
                .calledWith('new-bucket')
                .mockReturnValue(Promise.resolve(HttpStatusCodes.SUCCESS));

            // ACT
            const errorMessage = await BucketServices.createBucket('new-bucket');

            // ASSERT
            expect(errorMessage).toBeUndefined();
        });
    });

});