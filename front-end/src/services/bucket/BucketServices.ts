import BucketApi from '../../api/BucketApi';
import HttpStatusCodes from '../../helpers/HttpStatusCodes';
import BucketMapper from '../../mappers/bucket/BucketMapper';
import BucketErrors from '../../models/BucketErrors';
import BucketModel from '../../models/BucketModel';

async function getBuckets(): Promise<BucketModel[]> {
    const bucketDtos = await BucketApi.getBuckets();
    const bucketModels = bucketDtos.map(BucketMapper.get);

    return Promise.resolve(bucketModels);
}

async function createBucket(bucketName: string): Promise<string | undefined> {
    const statusCode = await BucketApi.createBucket(bucketName);
    return statusCode === HttpStatusCodes.SUCCESS ? undefined : BucketErrors.BUCKET_ALREADY_EXISTS;
}

export default {
    getBuckets,
    createBucket
};