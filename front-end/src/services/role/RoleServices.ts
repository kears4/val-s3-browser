import RolesApi from '../../api/RolesApi';
import RoleMapper from '../../mappers/role/RoleMapper';

async function getRoles() {
    const pagedResult = await RolesApi.getRoles();
    return pagedResult.resultats.map(r => RoleMapper.get(r));
}

export default {
    getRoles
};