import { when } from 'jest-when';
import RolesApi from '../../api/RolesApi';
import RoleMapper from '../../mappers/role/RoleMapper';

import PagedResultDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/PagedResultDtoObjectMother';
import RoleDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/role/RoleDtoObjectMother';
import RoleModelObjectMother from '../../tests/objectmothers/models/RoleModelObjectMother';
import RoleServices from './RoleServices';

describe('RoleServices tests', () => {

    describe('getRoles tests', () => {
        it('gets roles from api and maps the dto to model', async () => {
            // ARRANGE
            const roleADto = RoleDtoObjectMother.get(0);
            const roleBDto = RoleDtoObjectMother.get(1);

            const pagedResult = PagedResultDtoObjectMother.getWithResults([roleADto, roleBDto]);

            RolesApi.getRoles = jest.fn();
            when(RolesApi.getRoles as any)
                .calledWith()
                .mockReturnValue(pagedResult);

            const roleModelA = RoleModelObjectMother.get(0);
            const roleModelB = RoleModelObjectMother.get(1);

            RoleMapper.get = jest.fn();
            when(RoleMapper.get as any)
                .calledWith(roleADto)
                .mockReturnValue(roleModelA)
                .calledWith(roleBDto)
                .mockReturnValue(roleModelB);

            // ACT
            const result = await RoleServices.getRoles();

            // ASSERT
            expect(result).toEqual([roleModelA, roleModelB]);
        });
    });

});