import { when } from 'jest-when';

import IndividuApi from '../../api/IndividuApi';
import IndividuMapper from '../../mappers/individu/UserMapper';
import IndividuDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/individu/IndividuDtoObjectMother';
import PagedResultDtoObjectMother from '../../tests/objectmothers/dataTransferObjects/PagedResultDtoObjectMother';
import IndividuModelObjectMother from '../../tests/objectmothers/models/UserModelObjectMother';
import IndividuServices from './UserServices';

describe('UserServices tests', () => {

    describe('getUser tests', () => {
        it('gets user from api and maps the dto to model', async () => {
            // ARRANGE
            const individuDto = IndividuDtoObjectMother.get();
            const pagedResult = PagedResultDtoObjectMother.get(individuDto);

            IndividuApi.getIndividu = jest.fn();
            when(IndividuApi.getIndividu as any)
                .calledWith()
                .mockReturnValue(pagedResult);

            const individuModel = IndividuModelObjectMother.get();

            IndividuMapper.get = jest.fn();
            when(IndividuMapper.get as any)
                .calledWith(individuDto)
                .mockReturnValue(individuModel);

            // ACT
            const result = await IndividuServices.getUser();

            // ASSERT
            expect(result).toBe(individuModel);
        });
    });

});