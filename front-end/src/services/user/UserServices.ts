import IndividuApi from '../../api/IndividuApi';
import IndividuMapper from '../../mappers/individu/UserMapper';

async function getUser() {
    const pagedResult = await IndividuApi.getIndividu();
    const individuDto = pagedResult.resultats[0];
    const individuModel = IndividuMapper.get(individuDto);

    return individuModel;
}

export default {
    getUser
};