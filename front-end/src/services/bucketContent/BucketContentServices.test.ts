import { when } from 'jest-when';
import BucketContentApi from '../../api/BucketContentApi';
import BucketContentDto from '../../dataTransferObjects/BucketContentDto';
import BucketContentToFolderStucture from '../../domain/bucketContent/BucketContentToFolderStucture';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import FolderModelObjectMother from '../../tests/objectmothers/models/FolderModelObjectMother';
import BucketContentServices from './BucketContentServices';


describe('BucketContentServices tests', () => {
    describe('getBucketContent tests', () => {
        const BUCKET = BucketModelObjectMother.get('bucket');
        const ROOT_FOLDER = FolderModelObjectMother.get('root');
        const BUCKET_CONTENT = new Array<BucketContentDto>();

        it('get bucket folder content', async () => {
            // ARRANGE
            BucketContentApi.getBucketContent = jest.fn();
            when(BucketContentApi.getBucketContent as any)
                .calledWith(BUCKET.name)
                .mockReturnValue(BUCKET_CONTENT);

            BucketContentToFolderStucture.getRootFromContent = jest.fn().mockReturnValue(ROOT_FOLDER);

            // ACT
            const bucketFolderStructure = await BucketContentServices.getBucketContent(BUCKET);

            // ASSERT
            expect(bucketFolderStructure).toBe(ROOT_FOLDER);
            expect(BucketContentToFolderStucture.getRootFromContent).toHaveBeenCalledWith(BUCKET_CONTENT);
        });
    });

});