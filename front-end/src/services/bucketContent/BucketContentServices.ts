import BucketContentApi from '../../api/BucketContentApi';
import BucketContentToFolderStructure from '../../domain/bucketContent/BucketContentToFolderStucture';
import BucketModel from '../../models/BucketModel';
import FileModel from '../../models/FileModel';
import FolderModel from '../../models/FolderModel';

async function getBucketContent(bucket: BucketModel): Promise<FolderModel> {
    const bucketContent = await BucketContentApi.getBucketContent(bucket.name);
    const bucketFolderStructure = BucketContentToFolderStructure.getRootFromContent(bucketContent);

    return Promise.resolve(bucketFolderStructure);
}

function downloadFile(file: FileModel) {
    return;
}

export default {
    getBucketContent,
    downloadFile
};