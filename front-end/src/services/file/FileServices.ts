import FileApi from '../../api/FileApi';
import BucketModel from '../../models/BucketModel';
import FileModel from '../../models/FileModel';

async function getDownloadUrl(bucket: BucketModel, file: FileModel) {
    return await FileApi.getDownloadUrl(bucket.name, file.path);
}

async function uploadFileToS3(file: File, bucket: BucketModel, fileModel: FileModel) {
    const signedUploadUrl = await getUploadUrl(bucket, fileModel);

    console.warn(signedUploadUrl);

    const data = new FormData();
    data.append('file', file);

    await FileApi.uploadFileToS3(signedUploadUrl, data);
}

async function getUploadUrl(bucket: BucketModel, file: FileModel) {
    return await FileApi.getUploadUrl(bucket.name, file.path);
}



export default {
    getDownloadUrl,
    uploadFileToS3
};