import { when } from 'jest-when';
import FileApi from '../../api/FileApi';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import FileModelObjectMother from '../../tests/objectmothers/models/FileModelObjectMother';
import FileServices from './FileServices';

describe('FileServices tests', () => {
    it('getDownloadUrl gets signed download url', async () => {
        // ARRANGE
        const bucket = BucketModelObjectMother.get();
        const file = FileModelObjectMother.get();

        FileApi.getDownloadUrl = jest.fn();

        when(FileApi.getDownloadUrl as any)
            .calledWith(bucket.name, file.path)
            .mockReturnValue(Promise.resolve('www.download.com'));

        // ACT
        const downloadUrl = await FileServices.getDownloadUrl(bucket, file);

        // ASSERT
        expect(downloadUrl).toEqual('www.download.com');
    });

    it('uploadFileToS3 uploads signed url file to s3', async () => {
        // ARRANGE
        const file: File = { member: 'value' } as any;
        const bucket = BucketModelObjectMother.get();
        const fileModel = FileModelObjectMother.get();

        FileApi.getUploadUrl = jest.fn();
        FileApi.uploadFileToS3 = jest.fn();

        when(FileApi.getUploadUrl as any)
            .calledWith(bucket.name, fileModel.path)
            .mockReturnValue(Promise.resolve('www.upload.com'));

        // ACT
        await FileServices.uploadFileToS3(file, bucket, fileModel);

        // ASSERT
        const data = new FormData();
        data.append('file', file);

        expect(FileApi.uploadFileToS3).toHaveBeenCalledWith('www.upload.com', data);
    });
});