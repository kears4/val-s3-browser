import BucketsActionTypes from '../../actions/types/BucketsActionTypes';
import BucketsAction from './BucketsAction';
import BucketsState from './BucketsState';

const bucketsReducer = (state: BucketsState = new BucketsState(), action: BucketsAction): BucketsState => {
    switch (action.type) {
        case BucketsActionTypes.SET_ACTIVE_BUCKET:
            if (!action.bucket) return state;

            return { ...state, ...{ activeBucket: action.bucket } };

        case BucketsActionTypes.SET_BUCKETS:
            if (!action.buckets) return state;

            return { ...state, ...{ buckets: action.buckets } };

        case BucketsActionTypes.ADD_BUCKET:
            if (!action.bucket) return state;

            return { ...state, ...{ buckets: state.buckets.push(action.bucket) } };
        default:
            return state;
    }
};

export default bucketsReducer;
