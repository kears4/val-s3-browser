import bucketsReducer from './BucketsReducer';
import { List } from 'immutable';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import BucketsStateObjectMother from '../../tests/objectmothers/state/BucketsStateObjectMother';
import BucketsActionTypes from '../../actions/types/BucketsActionTypes';
import BucketModel from '../../models/BucketModel';

describe('FoldersReducer tests', () => {
    describe('SET_ACTIVE_BUCKET', () => {
        it('With active folder, Sets the active bucket', () => {
            // ARRANGE
            const bucket = BucketModelObjectMother.get();
            const state = BucketsStateObjectMother.get();

            // ACT
            const reducer = bucketsReducer(state, { type: BucketsActionTypes.SET_ACTIVE_BUCKET, bucket });

            // ASSERT
            expect(reducer.activeBucket).toBe(bucket);
        });

        it('Without active folder, returns same state', () => {
            // ARRANGE
            const state = BucketsStateObjectMother.get();

            // ACT
            const reducer = bucketsReducer(state, { type: BucketsActionTypes.SET_ACTIVE_BUCKET });

            // ASSERT
            expect(reducer).toBe(state);
        });
    });

    describe('SET_BUCKETS', () => {
        it('With buckets, sets buckets', () => {
            // ARRANGE
            const bucketA = BucketModelObjectMother.get('bucketA');
            const bucketB = BucketModelObjectMother.get('bucketB');
            const bucketC = BucketModelObjectMother.get('bucketC');

            const buckets = List<BucketModel>([bucketA, bucketB, bucketC]);

            const state = BucketsStateObjectMother.get();

            // ACT
            const reducer = bucketsReducer(state, { type: BucketsActionTypes.SET_BUCKETS, buckets });

            // ASSERT
            expect(reducer.buckets).toBe(buckets);
        });

        it('Without buckets, returns same state', () => {
            // ARRANGE
            const state = BucketsStateObjectMother.get();

            // ACT
            const reducer = bucketsReducer(state, { type: BucketsActionTypes.SET_BUCKETS });

            // ASSERT
            expect(reducer).toBe(state);
        });
    });

    describe('ADD_BUCKET', () => {
        it('with no bucket, returns same state', () => {
            const state = BucketsStateObjectMother.get();

            // ACT
            const reducer = bucketsReducer(state, { type: BucketsActionTypes.ADD_BUCKET });

            // ASSERT
            expect(reducer).toBe(state);
        });

        it('with bucket and existing buckets, returns buckets with newly added bucket', () => {
            const bucketA = BucketModelObjectMother.get('bucketA');
            const state = BucketsStateObjectMother.getWithBuckets([bucketA]);

            const newBucket = BucketModelObjectMother.get('newBucket');

            // ACT
            const reducer = bucketsReducer(state, { type: BucketsActionTypes.ADD_BUCKET, bucket: newBucket });

            // ASSERT
            expect(reducer.buckets).toEqual(List([bucketA, newBucket]));
        });
    });
});