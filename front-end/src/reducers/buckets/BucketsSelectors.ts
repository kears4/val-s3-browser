import State from '../../State';

function isOnActiveBucket(state: State) {
    return state.buckets.activeBucket !== undefined;
}

export default {
    isOnActiveBucket
};