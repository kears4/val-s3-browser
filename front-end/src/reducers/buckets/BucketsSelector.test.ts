import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import BucketsStateObjectMother from '../../tests/objectmothers/state/BucketsStateObjectMother';
import StateObjectMother from '../../tests/objectmothers/state/StateObjectMother';
import BucketsSelectors from './BucketsSelectors';

describe('BucketsSelector tests', () => {
    const BUCKET_A = BucketModelObjectMother.get('BucketA');
    const BUCKET_B = BucketModelObjectMother.get('BucketB');

    const BUCKETS = [BUCKET_A, BUCKET_B];

    describe('isOnActiveBucket tests', () => {
        it('with no buckets, is not on active bucket', () => {
            // ARRANGE
            const foldersState = BucketsStateObjectMother.get();
            const state = StateObjectMother.getWithBucketsState(foldersState);

            // ACT
            const isOnActiveBucket = BucketsSelectors.isOnActiveBucket(state);

            // ASSERT
            expect(isOnActiveBucket).toBeFalsy();
        });

        it('with buckets, none active, is not on active bucket', () => {
            // ARRANGE
            const foldersState = BucketsStateObjectMother.getWithBuckets(BUCKETS);
            const state = StateObjectMother.getWithBucketsState(foldersState);

            // ACT
            const isOnActiveBucket = BucketsSelectors.isOnActiveBucket(state);

            // ASSERT
            expect(isOnActiveBucket).toBeFalsy();
        });

        it('with buckets, with active bucket, is on active bucket', () => {
            // ARRANGE
            const foldersState = BucketsStateObjectMother.getWithActiveBucket(BUCKET_B);
            const state = StateObjectMother.getWithBucketsState(foldersState);

            // ACT
            const isOnActiveBucket = BucketsSelectors.isOnActiveBucket(state);

            // ASSERT
            expect(isOnActiveBucket).toBeTruthy();
        });
    });
});