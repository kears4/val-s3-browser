import { List } from 'immutable';
import BucketModel from '../../models/BucketModel';
import FolderModel from '../../models/FolderModel';

export default class BucketsState {
    buckets: List<BucketModel>;
    activeBucket?: BucketModel;

    constructor() {
        this.buckets = List<FolderModel>();
    }
}