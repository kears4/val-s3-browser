import { List } from 'immutable';
import { Action } from 'redux';
import BucketModel from '../../models/BucketModel';

export default interface BucketsAction extends Action {
    bucket?: BucketModel;
    buckets?: List<BucketModel>;
}