import { List } from 'immutable';
import RolesStateObjectMother from '../../tests/objectmothers/state/RolesStateObjectMother';
import RoleActionTypes from '../../actions/types/RoleActionTypes';
import RoleModelObjectMother from '../../tests/objectmothers/models/RoleModelObjectMother';
import RoleModel from '../../models/RoleModel';
import rolesReducer from './RolesReducer';

describe('RolesReducer tests', () => {
    describe('SET_ROLES', () => {
        it('With roles, sets roles', () => {
            // ARRANGE
            const roleA = RoleModelObjectMother.get(0);
            const roleB = RoleModelObjectMother.get(1);

            const state = RolesStateObjectMother.get();

            const roles = List<RoleModel>([roleA, roleB]);

            // ACT
            const reducer = rolesReducer(state, { type: RoleActionTypes.SET_ROLES, roles });

            // ASSERT
            expect(reducer.roles).toBe(roles);
        });

        it('Without roles, returns same state', () => {
            // ARRANGE
            const state = RolesStateObjectMother.get();

            // ACT
            const reducer = rolesReducer(state, { type: RoleActionTypes.SET_ROLES });

            // ASSERT
            expect(reducer).toBe(state);
        });
    });
});