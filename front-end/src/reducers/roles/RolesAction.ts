import { List } from 'immutable';
import { Action } from 'redux';
import RoleModel from '../../models/RoleModel';

export default interface RolesAction extends Action {
    roles?: List<RoleModel>;
}