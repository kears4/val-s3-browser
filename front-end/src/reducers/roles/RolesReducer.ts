import RoleActionTypes from '../../actions/types/RoleActionTypes';
import RolesAction from './RolesAction';
import RolesState from './RolesState';

const rolesReducer = (state: RolesState = new RolesState(), action: RolesAction): RolesState => {
    switch (action.type) {
        case RoleActionTypes.SET_ROLES:
            if (!action.roles) return state;

            return { ...state, ...{ roles: action.roles } };

        default:
            return state;
    }
};

export default rolesReducer;
