import { List } from 'immutable';
import RoleModel from '../../models/RoleModel';

export default class RolesState {
    roles: List<RoleModel>;

    constructor() {
        this.roles = List<RoleModel>();
    }
}