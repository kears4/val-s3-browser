import FolderModelObjectMother from '../../tests/objectmothers/models/FolderModelObjectMother';
import FoldersStateObjectMother from '../../tests/objectmothers/state/FoldersStateObjectMother';
import StateObjectMother from '../../tests/objectmothers/state/StateObjectMother';
import FoldersSelectors from './FoldersSelectors';

describe('FoldersSelectors tests', () => {
    describe('getCurrentFolder tests', () => {
        it('With no folder in path, returns undefined', () => {
            // ARRANGE
            const foldersState = FoldersStateObjectMother.getWithPath([]);
            const state = StateObjectMother.getWithFoldersState(foldersState);

            // ACT
            const currentFolder = FoldersSelectors.getCurrentFolder(state);

            // ASSERT
            expect(currentFolder).toBe(undefined);
        });

        it('With one folder in path, returns folder', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();
            const foldersState = FoldersStateObjectMother.getWithPath([folder]);
            const state = StateObjectMother.getWithFoldersState(foldersState);

            // ACT
            const currentFolder = FoldersSelectors.getCurrentFolder(state);

            // ASSERT
            expect(currentFolder).toBe(folder);
        });

        it('With three folders in path, returns last folder', () => {
            // ARRANGE
            const folderA = FolderModelObjectMother.get('folderA');
            const folderB = FolderModelObjectMother.get('folderB');
            const folderC = FolderModelObjectMother.get('folderC');

            const foldersState = FoldersStateObjectMother.getWithPath([folderA, folderB, folderC]);
            const state = StateObjectMother.getWithFoldersState(foldersState);

            // ACT
            const currentFolder = FoldersSelectors.getCurrentFolder(state);

            // ASSERT
            expect(currentFolder).toBe(folderC);
        });
    });

    describe('isAtRoot tests', () => {
        it('with no folders in path, is at root', () => {
            // ARRANGE
            const foldersState = FoldersStateObjectMother.getWithPath([]);
            const state = StateObjectMother.getWithFoldersState(foldersState);

            // ACT
            const isAtRootFolder = FoldersSelectors.isAtRootFolder(state);

            // ASSERT
            expect(isAtRootFolder).toBeTruthy();
        });

        it('with one folder in path, is at root', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();
            const foldersState = FoldersStateObjectMother.getWithPath([folder]);
            const state = StateObjectMother.getWithFoldersState(foldersState);

            // ACT
            const isAtRootFolder = FoldersSelectors.isAtRootFolder(state);

            // ASSERT
            expect(isAtRootFolder).toBeTruthy();
        });

        it('with two folders in path, is not at root', () => {
            // ARRANGE
            const folderA = FolderModelObjectMother.get();
            const folderB = FolderModelObjectMother.get();

            const foldersState = FoldersStateObjectMother.getWithPath([folderA, folderB]);
            const state = StateObjectMother.getWithFoldersState(foldersState);

            // ACT
            const isAtRootFolder = FoldersSelectors.isAtRootFolder(state);

            // ASSERT
            expect(isAtRootFolder).toBeFalsy();
        });
    });
});