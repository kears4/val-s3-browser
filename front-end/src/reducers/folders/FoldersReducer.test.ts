import foldersReducer from './FoldersReducer';
import FolderModelObjectMother from '../../tests/objectmothers/models/FolderModelObjectMother';
import FoldersActionTypes from '../../actions/types/FoldersActionTypes';
import FoldersStateObjectMother from '../../tests/objectmothers/state/FoldersStateObjectMother';
import { List } from 'immutable';
import FolderModel from '../../models/FolderModel';
import FileModelObjectMother from '../../tests/objectmothers/models/FileModelObjectMother';

describe('FoldersReducer tests', () => {
    describe('OPEN_FOLDER', () => {
        it('With no folder state stays the same', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();
            const state = FoldersStateObjectMother.getWithPath([folder]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.OPEN_FOLDER });

            // ASSERT
            expect(reducer).toBe(state);
        });

        it('Can start path', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();

            // ACT
            const reducer = foldersReducer(undefined, { type: FoldersActionTypes.OPEN_FOLDER, folder });

            // ASSERT
            expect(reducer.path).toEqual(List<FolderModel>([folder]));
        });

        it('Can continu path', () => {
            // ARRANGE
            const folderA = FolderModelObjectMother.get('folderA');
            const folderB = FolderModelObjectMother.get('folderB');
            const folderC = FolderModelObjectMother.get('folderC');

            const state = FoldersStateObjectMother.getWithPath([folderA, folderB]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.OPEN_FOLDER, folder: folderC });

            // ASSERT
            expect(reducer.path).toEqual(List<FolderModel>([folderA, folderB, folderC]));
        });
    });


    describe('BROWSE_TO_PARENT', () => {
        it('Returns to parent folder', () => {
            // ARRANGE
            const folderA = FolderModelObjectMother.get('folderA');
            const folderB = FolderModelObjectMother.get('folderB');
            const folderC = FolderModelObjectMother.get('folderC');

            const state = FoldersStateObjectMother.getWithPath([folderA, folderB, folderC]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.BROWSE_TO_PARENT });

            // ASSERT
            expect(reducer.path).toEqual(List<FolderModel>([folderA, folderB]));
        });
    });

    describe('OPEN_BUCKET_FOLDER', () => {
        it('With no folder state stays the same', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();
            const state = FoldersStateObjectMother.getWithPath([folder]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.OPEN_BUCKET_FOLDER });

            // ASSERT
            expect(reducer).toBe(state);
        });

        it('Can start path', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();

            // ACT
            const reducer = foldersReducer(undefined, { type: FoldersActionTypes.OPEN_BUCKET_FOLDER, folder });

            // ASSERT
            expect(reducer.path).toEqual(List<FolderModel>([folder]));
        });

        it('Always starts path path', () => {
            // ARRANGE
            const folderA = FolderModelObjectMother.get('folderA');
            const folderB = FolderModelObjectMother.get('folderB');
            const folderC = FolderModelObjectMother.get('folderC');

            const state = FoldersStateObjectMother.getWithPath([folderA, folderB]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.OPEN_BUCKET_FOLDER, folder: folderC });

            // ASSERT
            expect(reducer.path).toEqual(List<FolderModel>([folderC]));
        });
    });

    describe('ADD_FILE', () => {
        it('With one folder, no file is added, state is same', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();

            const state = FoldersStateObjectMother.getWithPath([folder]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.ADD_FILE });

            // ASSERT
            expect(reducer).toBe(state);
        });

        it('With one folder, file is added to folder', () => {
            // ARRANGE
            const folder = FolderModelObjectMother.get();
            const addedFile = FileModelObjectMother.get();

            const state = FoldersStateObjectMother.getWithPath([folder]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.ADD_FILE, addedFile });

            // ASSERT
            const expectedFolderAugmented = { ...folder, ...{ content: [addedFile] } };
            expect(reducer.path).toEqual(List<FolderModel>([expectedFolderAugmented]));
        });

        it('With one folder, file in folder, new file added at end', () => {
            // ARRANGE
            const file = FileModelObjectMother.get('file');
            const folder = FolderModelObjectMother.getWithContent([file]);
            const addedFile = FileModelObjectMother.get('addedFile');

            const state = FoldersStateObjectMother.getWithPath([folder]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.ADD_FILE, addedFile });

            // ASSERT
            const expectedFolderAugmented = { ...folder, ...{ content: [...folder.content, addedFile] } };
            expect(reducer.path).toEqual(List<FolderModel>([expectedFolderAugmented]));
        });

        it('With three folders in path, file is added at end of path', () => {
            // ARRANGE
            const folderA = FolderModelObjectMother.get('folderA');
            const folderB = FolderModelObjectMother.get('folderB');
            const folderC = FolderModelObjectMother.get('folderC');

            const addedFile = FileModelObjectMother.get();


            const state = FoldersStateObjectMother.getWithPath([folderA, folderB, folderC]);

            // ACT
            const reducer = foldersReducer(state, { type: FoldersActionTypes.ADD_FILE, addedFile });

            // ASSERT
            const expectedFolderCAugmented = { ...folderC, ...{ content: [addedFile] } };
            expect(reducer.path).toEqual(List<FolderModel>([folderA, folderB, expectedFolderCAugmented]));
        });
    });
});