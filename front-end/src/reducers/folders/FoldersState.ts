import { List } from 'immutable';
import FolderModel from '../../models/FolderModel';

export default class FoldersState {
    path: List<FolderModel>;

    constructor() {
        this.path = List<FolderModel>();
    }
}