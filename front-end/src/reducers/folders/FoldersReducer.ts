import FoldersState from './FoldersState';
import FoldersAction from './FoldersAction';
import FoldersActionTypes from '../../actions/types/FoldersActionTypes';
import { List } from 'immutable';
import FolderModel from '../../models/FolderModel';

const foldersReducer = (state: FoldersState = new FoldersState(), action: FoldersAction): FoldersState => {
    switch (action.type) {
        case FoldersActionTypes.OPEN_FOLDER:
            if (!action.folder) return state;

            const newPath = state.path.push(action.folder);
            return { ...state, ...{ path: newPath } };

        case FoldersActionTypes.BROWSE_TO_PARENT:
            const pathLastRemoved = state.path.remove(state.path.count() - 1);
            return { ...state, ...{ path: pathLastRemoved } };

        case FoldersActionTypes.OPEN_BUCKET_FOLDER:
            if (!action.folder) return state;

            const pathWithBrowserBucket = List<FolderModel>([action.folder]);
            return { ...state, ...{ path: pathWithBrowserBucket } };

        case FoldersActionTypes.ADD_FILE:
            if (!action.addedFile) return state;
            const addedFile = action.addedFile;

            const openFolder = state.path.last(new FolderModel([], 'folder'));

            const newPathFileAdded = state.path.map((folder) => {
                if (folder !== openFolder) return folder;
                return { ...folder, ...{ content: [...folder.content, addedFile] } };
            });

            return { ...state, ...{ path: newPathFileAdded } };
        default:
            return state;
    }
};

export default foldersReducer;
