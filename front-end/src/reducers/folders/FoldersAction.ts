import { Action } from 'redux';
import FileModel from '../../models/FileModel';
import FolderModel from '../../models/FolderModel';

export default interface FoldersAction extends Action {
    folder?: FolderModel;
    addedFile?: FileModel;
}