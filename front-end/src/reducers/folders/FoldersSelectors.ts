import State from '../../State';

const getCurrentFolder = (state: State) => {
    return state.folders.path.last(undefined);
};

const isAtRootFolder = (state: State) => {
    return state.folders.path.count() <= 1;
};

export default {
    getCurrentFolder,
    isAtRootFolder
};