import UserActionTypes from '../../actions/types/UserActionTypes';
import UserAction from './UserAction';
import UserState from './UserState';

const userReducer = (state: UserState = new UserState(), action: UserAction): UserState => {
    switch (action.type) {
        case UserActionTypes.SET_TOKEN:
            if (!action.token) return state;

            return { ...state, ...{ token: action.token } };

        case UserActionTypes.SET_USER:
            if (!action.user) return state;

            return { ...state, ...{ user: action.user } };
        default:
            return state;
    }
};

export default userReducer;