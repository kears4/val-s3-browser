import RoleModelObjectMother from '../../tests/objectmothers/models/RoleModelObjectMother';
import UserModelObjectMother from '../../tests/objectmothers/models/UserModelObjectMother';
import RolesStateObjectMother from '../../tests/objectmothers/state/RolesStateObjectMother';
import StateObjectMother from '../../tests/objectmothers/state/StateObjectMother';
import UserStateObjectMother from '../../tests/objectmothers/state/UserStateObjectMother';
import UserSelectors from './UserSelectors';

describe('UserSelectors tests', () => {
    describe('getUserRoleName tests', () => {
        const ADMIN_ROLE = RoleModelObjectMother.get(0, 'ADMIN', 'Admin');
        const COLLABO_ROLE = RoleModelObjectMother.get(1, 'COLLABO', 'Collaborateur');

        it('With no user, returns empty string', () => {
            // ARRANGE
            const userState = UserStateObjectMother.get();
            const rolesState = RolesStateObjectMother.getWithRoles([ADMIN_ROLE, COLLABO_ROLE]);

            const state = StateObjectMother.getWithUserAndRolesState(userState, rolesState);

            // ACT
            const userRoleName = UserSelectors.getUserRoleName(state);

            // ASSERT
            expect(userRoleName).toEqual('');
        });

        it('With user, no roles, returns empty string', () => {
            // ARRANGE
            const user = UserModelObjectMother.get('', '', '', '', ADMIN_ROLE.idRole);
            const userState = UserStateObjectMother.getWithUserModel(user);
            const rolesState = RolesStateObjectMother.get();

            const state = StateObjectMother.getWithUserAndRolesState(userState, rolesState);

            // ACT
            const userRoleName = UserSelectors.getUserRoleName(state);

            // ASSERT
            expect(userRoleName).toEqual('');
        });

        it('With user, user role does not exist, returns empty string', () => {
            // ARRANGE
            const user = UserModelObjectMother.get('', '', '', '', ADMIN_ROLE.idRole);
            const userState = UserStateObjectMother.getWithUserModel(user);
            const rolesState = RolesStateObjectMother.getWithRoles([COLLABO_ROLE]);

            const state = StateObjectMother.getWithUserAndRolesState(userState, rolesState);

            // ACT
            const userRoleName = UserSelectors.getUserRoleName(state);

            // ASSERT
            expect(userRoleName).toEqual('');
        });

        it('With user, user role does exist, returns role name', () => {
            // ARRANGE
            const user = UserModelObjectMother.get('', '', '', '', COLLABO_ROLE.idRole);
            const userState = UserStateObjectMother.getWithUserModel(user);
            const rolesState = RolesStateObjectMother.getWithRoles([ADMIN_ROLE, COLLABO_ROLE]);

            const state = StateObjectMother.getWithUserAndRolesState(userState, rolesState);

            // ACT
            const userRoleName = UserSelectors.getUserRoleName(state);

            // ASSERT
            expect(userRoleName).toEqual(COLLABO_ROLE.nom);
        });
    });
});