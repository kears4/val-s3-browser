import RoleModel from '../../models/RoleModel';
import State from '../../State';

function getUserRoleName(state: State): string {
    const user = state.user.user;
    if (!user) return '';

    const roles = state.roles.roles;
    if (roles.count() === 0) return '';

    const emptyRole = new RoleModel(-1, '', '');
    const userRole = roles.filter(r => r.idRole === user.roleId).first(emptyRole);

    return userRole.nom;
}

export default {
    getUserRoleName
};