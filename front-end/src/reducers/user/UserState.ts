import UserModel from '../../models/UserModel';

export default class UserState {
    token: string;
    user: UserModel;

    public constructor() {
        this.token = '';
        this.user = new UserModel('', '', '', '', -1);
    }
}