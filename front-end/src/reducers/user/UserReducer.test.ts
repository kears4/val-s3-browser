import UserActionTypes from '../../actions/types/UserActionTypes';
import UserModelObjectMother from '../../tests/objectmothers/models/UserModelObjectMother';
import UserStateObjectMother from '../../tests/objectmothers/state/UserStateObjectMother';
import userReducer from './UserReducer';

describe('UserReducer tests', () => {
    describe('SET_TOKEN', () => {
        it('sets token', () => {
            // ARRANGE
            const state = UserStateObjectMother.get();
            const token = 'token';

            // ACT
            const reducer = userReducer(state, { type: UserActionTypes.SET_TOKEN, token });

            // ASSERT
            expect(reducer.token).toEqual(token);
        });

        it('with no token returns same state', () => {
            // ARRANGE
            const state = UserStateObjectMother.get();

            // ACT
            const reducer = userReducer(state, { type: UserActionTypes.SET_TOKEN });

            // ASSERT
            expect(reducer).toBe(state);
        });
    });

    describe('SET_USER', () => {
        it('sets user', () => {
            // ARRANGE
            const state = UserStateObjectMother.get();
            const user = UserModelObjectMother.get();

            // ACT
            const reducer = userReducer(state, { type: UserActionTypes.SET_USER, user });

            // ASSERT
            expect(reducer.user).toEqual(user);
        });

        it('with no user returns same state', () => {
            // ARRANGE
            const state = UserStateObjectMother.get();

            // ACT
            const reducer = userReducer(state, { type: UserActionTypes.SET_USER });

            // ASSERT
            expect(reducer).toBe(state);
        });
    });
});