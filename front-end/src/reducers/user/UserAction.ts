import { Action } from 'redux';
import UserModel from '../../models/UserModel';

export default interface UserAction extends Action {
    token?: string;
    user?: UserModel;
}