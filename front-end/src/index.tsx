import * as serviceWorker from './serviceWorker';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import store from './Store';
import Keycloak from './Keycloak';
import UserActions from './actions/user/UserActions';
import BucketsActions from './actions/buckets/BucketsActions';

import './index.css';
import RoleActions from './actions/roles/RoleActions';


Keycloak.init({ onLoad: 'login-required' }).then(async (authenticated) => {
    const token = Keycloak.token ? Keycloak.token : '';
    await store.dispatch(UserActions.setToken(token));

    UserActions.getUser(store.dispatch);
    RoleActions.getRoles(store.dispatch);
    BucketsActions.getBuckets(store.dispatch);

    ReactDOM.render(
        <Provider store={store}>
            <App logout={Keycloak.logout} />
        </Provider>,

        document.querySelector('#root'),
    );
}).catch((error) => {
    console.error(`Keycloak error: ${error}`);
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
