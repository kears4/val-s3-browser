import React from 'react';
import Container from '@material-ui/core/Container';
import { Box } from '@material-ui/core';
import BucketBrowserContainer from '../../containers/fileBrowser/bucketBrowser/BucketBrowserContainer';
import FileBrowser from '../../components/fileBrowser/FileBrowser';
import { Helmet } from 'react-helmet';
import FileToolbar from '../../components/fileBrowser/fileToolbar/FileToolbar';

export default function FileBrowserPage() {
    return (
        <>
            <Helmet>
                <title>Fichiers | S3-BROWSER</title>
            </Helmet>

            <Box
                sx={{
                    backgroundColor: 'background.default',
                    minHeight: '100%',
                    py: 3
                }}
            >
                <Container maxWidth={false}>
                    <FileToolbar data-test-id='fileToolbar' />
                    <BucketBrowserContainer data-test-id='bucketBrowser' />

                    <Box sx={{ pt: 3 }}>
                        <FileBrowser data-test-id='fileBrowser' />
                    </Box>
                </Container>
            </Box>
        </>
    );
}