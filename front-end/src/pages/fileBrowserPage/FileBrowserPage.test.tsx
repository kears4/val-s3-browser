import React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import { find } from '../../tests/utils/FindHelpers';
import FileBrowserPage from './FileBrowserPage';


describe('<FileBrowserPage />', () => {
    let wrapper: ShallowWrapper<AnalyserNode, any, any>;

    beforeEach(() => {
        wrapper = shallow(<FileBrowserPage />);
    });

    it('has fileToolbar', () => {
        const fileToolbar = find(wrapper, 'fileToolbar');
        expect(fileToolbar.exists()).toBeTruthy();
    });

    it('has bucketBrowser', () => {
        expect(find(wrapper, 'bucketBrowser').exists()).toBeTruthy();
    });

    it('has fileBrowser', () => {
        expect(find(wrapper, 'fileBrowser').exists()).toBeTruthy();
    });
});
