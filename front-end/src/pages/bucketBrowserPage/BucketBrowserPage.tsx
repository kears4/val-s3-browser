import React from 'react';
import { Helmet } from 'react-helmet';

import Container from '@material-ui/core/Container';
import { Box } from '@material-ui/core';
import BucketToolbar from '../../components/buckets/bucketToolbar/BucketToolbar';
import BucketContainer from '../../containers/buckets/BucketContainer';

export default function BucketBrowserPage() {
    return (
        <>
            <Helmet>
                <title>Compartiments | S3-BROWSER</title>
            </Helmet>
            <Box
                sx={{
                    backgroundColor: 'background.default',
                    minHeight: '100%',
                    py: 3
                }}
            >
                <Container maxWidth={false}>
                    <BucketToolbar data-test-id='bucketToolbar' />
                    <Box sx={{ pt: 3 }}>
                        <BucketContainer data-test-id='buckets' />
                    </Box>
                </Container>
            </Box>
        </>
    );
}