import React from 'react';

import { shallow, ShallowWrapper } from 'enzyme';
import { find } from '../../tests/utils/FindHelpers';
import BucketBrowserPage from './BucketBrowserPage';


describe('<BucketBrowserPage />', () => {
    let wrapper: ShallowWrapper<AnalyserNode, any, any>;

    beforeEach(() => {
        wrapper = shallow(<BucketBrowserPage />);
    });

    it('has bucketToolbar', () => {
        const bucketToolbar = find(wrapper, 'bucketToolbar');
        expect(bucketToolbar.exists()).toBeTruthy();
    });

    it('has buckets component', () => {
        const buckets = find(wrapper, 'buckets');
        expect(buckets.exists()).toBeTruthy();
    });
});
