import { Dispatch } from 'redux';
import Constants from '../../domain/Constants';
import BucketModel from '../../models/BucketModel';
import FileModel from '../../models/FileModel';
import FolderModel from '../../models/FolderModel';
import FoldersAction from '../../reducers/folders/FoldersAction';
import FoldersState from '../../reducers/folders/FoldersState';
import FileServices from '../../services/file/FileServices';
import FoldersActionTypes from '../types/FoldersActionTypes';

function openFolder(folder: FolderModel): FoldersAction {
	return {
		type: FoldersActionTypes.OPEN_FOLDER,
		folder
	};
}

function browseToParent(): FoldersAction {
	return {
		type: FoldersActionTypes.BROWSE_TO_PARENT,
	};
}

function openBucketFolder(folder: FolderModel): FoldersAction {
	return {
		type: FoldersActionTypes.OPEN_BUCKET_FOLDER,
		folder
	};
}

async function uploadFile(dispatch: Dispatch<FoldersAction>,
	folderState: FoldersState,
	bucket: BucketModel,
	file: File) {

	const pathRootRemoved = folderState.path.filter(f => f.name !== Constants.rootFolderName);
	const filePath = [...pathRootRemoved.map(p => p.name).toArray(), file.name].join('/');
	const uploadedFileModel = new FileModel(file.name, filePath);

	await FileServices.uploadFileToS3(file, bucket, uploadedFileModel);
	dispatch({
		type: FoldersActionTypes.ADD_FILE,
		addedFile: uploadedFileModel
	});
}

export default {
	openFolder,
	browseToParent,
	openBucketFolder,
	uploadFile
};