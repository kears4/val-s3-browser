import FileServices from '../../services/file/FileServices';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import FileModelObjectMother from '../../tests/objectmothers/models/FileModelObjectMother';
import FolderModelObjectMother from '../../tests/objectmothers/models/FolderModelObjectMother';
import FoldersStateObjectMother from '../../tests/objectmothers/state/FoldersStateObjectMother';
import FoldersActionTypes from '../types/FoldersActionTypes';
import FoldersActions from './FoldersActions';

describe('FoldersActions tests', () => {
    const ROOT_FOLDER = FolderModelObjectMother.get('root');

    describe('openFolder', () => {
        it('sets active folder', () => {
            // ARRANGE
            const FOLDER = FolderModelObjectMother.get();

            // ACT
            const action = FoldersActions.openFolder(FOLDER);

            // ASSERT
            expect(action).toEqual({
                type: FoldersActionTypes.OPEN_FOLDER,
                folder: FOLDER
            });
        });
    });

    describe('browseToParent', () => {
        it('browse to parent', () => {
            const action = FoldersActions.browseToParent();

            expect(action).toEqual({
                type: FoldersActionTypes.BROWSE_TO_PARENT,
            });
        });
    });

    describe('openBucketFolder', () => {
        it('sets active bucket folder', () => {
            // ARRANGE
            const FOLDER = FolderModelObjectMother.get();

            // ACT
            const action = FoldersActions.openBucketFolder(FOLDER);

            // ASSERT
            expect(action).toEqual({
                type: FoldersActionTypes.OPEN_BUCKET_FOLDER,
                folder: FOLDER
            });
        });
    });

    describe('uploadFile', () => {
        const BUCKET = BucketModelObjectMother.get('bucket');
        const FILE: File = { name: 'file.mid' } as any;

        let dispatch: jest.Mock;

        beforeEach(() => {
            dispatch = jest.fn();
            FileServices.uploadFileToS3 = jest.fn();
        });

        it('at root folder, creates new file model and upload it with simple path', async () => {
            // ARRANGE
            const folderState = FoldersStateObjectMother.getWithPath([ROOT_FOLDER]);

            // ACT
            await FoldersActions.uploadFile(dispatch, folderState, BUCKET, FILE);

            // ASSERT
            const expectedFileUploaded = FileModelObjectMother.get('file.mid', 'file.mid');

            expect(FileServices.uploadFileToS3).toHaveBeenCalledWith(FILE, BUCKET, expectedFileUploaded);
            expect(dispatch)
                .toHaveBeenCalledWith({ type: FoldersActionTypes.ADD_FILE, addedFile: expectedFileUploaded });
        });

        it('at second level path, creates new file model and upload it with path', async () => {
            // ARRANGE
            const folderState = FoldersStateObjectMother.getWithPath([
                ROOT_FOLDER,
                FolderModelObjectMother.get('folderA'),
                FolderModelObjectMother.get('folderB'),
            ]);

            // ACT
            await FoldersActions.uploadFile(dispatch, folderState, BUCKET, FILE);

            // ASSERT
            const expectedFileUploaded = FileModelObjectMother.get('file.mid', 'folderA/folderB/file.mid');

            expect(FileServices.uploadFileToS3).toHaveBeenCalledWith(FILE, BUCKET, expectedFileUploaded);
            expect(dispatch)
                .toHaveBeenCalledWith({ type: FoldersActionTypes.ADD_FILE, addedFile: expectedFileUploaded });
        });
    });
});