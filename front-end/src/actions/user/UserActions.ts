import { Dispatch } from 'redux';
import UserAction from '../../reducers/user/UserAction';
import UserServices from '../../services/user/UserServices';
import TokenProvider from '../../TokenProvider';
import UserActionTypes from '../types/UserActionTypes';

function setToken(token: string) {
    TokenProvider.registerToken(token);

    return {
        type: UserActionTypes.SET_TOKEN,
        token
    };
}

async function getUser(dispatch: Dispatch<UserAction>) {
    const user = await UserServices.getUser();

    dispatch({
        type: UserActionTypes.SET_USER,
        user
    });
}

export default {
    setToken,
    getUser
};