import { when } from 'jest-when';
import UserServices from '../../services/user/UserServices';
import UserModelObjectMother from '../../tests/objectmothers/models/UserModelObjectMother';
import TokenProvider from '../../TokenProvider';
import UserActionTypes from '../types/UserActionTypes';
import UserActions from './UserActions';

describe('UserActions tests', () => {
    describe('setToken', () => {
        it('sets token to user state', () => {
            // ARRANGE
            const token = 'token';

            // ACT
            const action = UserActions.setToken(token);

            // ASSERT
            expect(action).toEqual({
                type: UserActionTypes.SET_TOKEN,
                token
            });
        });

        it('sets token in token provider', async () => {
            // ARRANGE
            const token = 'token';
            TokenProvider.registerToken = jest.fn();

            // ACT
            UserActions.setToken(token);

            // ASSERT
            expect(TokenProvider.registerToken).toHaveBeenCalledWith(token);
        });
    });

    describe('getUser', () => {
        it('sets user from services', async () => {
            // ARRANGE
            const user = UserModelObjectMother.get();

            UserServices.getUser = jest.fn();
            when(UserServices.getUser as any)
                .calledWith()
                .mockReturnValue(user);

            const dispatch = jest.fn();

            // ACT
            await UserActions.getUser(dispatch);

            // ASSERT
            expect(dispatch).toHaveBeenCalledWith({
                type: UserActionTypes.SET_USER,
                user
            });
        });
    });
});