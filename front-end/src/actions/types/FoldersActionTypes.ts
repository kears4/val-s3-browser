export default {
    OPEN_FOLDER: 'OPEN_FOLDER',
    BROWSE_TO_PARENT: 'BROWSE_TO_PARENT',
    OPEN_BUCKET_FOLDER: 'OPEN_BUCKET_FOLDER',
    ADD_FILE: 'ADD_FILE'
};