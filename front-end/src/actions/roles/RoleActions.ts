import { List } from 'immutable';
import { Dispatch } from 'redux';
import UserAction from '../../reducers/user/UserAction';
import RoleServices from '../../services/role/RoleServices';
import RoleActionTypes from '../types/RoleActionTypes';


async function getRoles(dispatch: Dispatch<UserAction>) {
    const roles = List(await RoleServices.getRoles());

    dispatch({
        type: RoleActionTypes.SET_ROLES,
        roles
    });
}

export default {
    getRoles
};