import { List } from 'immutable';
import { when } from 'jest-when';
import RoleServices from '../../services/role/RoleServices';
import RoleModelObjectMother from '../../tests/objectmothers/models/RoleModelObjectMother';
import RoleActionTypes from '../types/RoleActionTypes';
import RoleActions from './RoleActions';

describe('RoleActions tests', () => {
    describe('getRoles', () => {
        it('sets roles from services', async () => {
            // ARRANGE
            const roleA = RoleModelObjectMother.get();
            const roleB = RoleModelObjectMother.get();

            RoleServices.getRoles = jest.fn();
            when(RoleServices.getRoles as any)
                .calledWith()
                .mockReturnValue([roleA, roleB]);

            const dispatch = jest.fn();

            // ACT
            await RoleActions.getRoles(dispatch);

            // ASSERT
            expect(dispatch).toHaveBeenCalledWith({
                type: RoleActionTypes.SET_ROLES,
                roles: List([roleA, roleB])
            });
        });
    });
});