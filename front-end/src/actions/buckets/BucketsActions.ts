import { List } from 'immutable';
import { Dispatch } from 'redux';
import BucketModel from '../../models/BucketModel';
import BucketsAction from '../../reducers/buckets/BucketsAction';
import BucketServices from '../../services/bucket/BucketServices';
import BucketContentServices from '../../services/bucketContent/BucketContentServices';
import FoldersActions from '../folders/FoldersActions';
import BucketsActionTypes from '../types/BucketsActionTypes';

async function openBucket(dispatch: Dispatch<BucketsAction>, bucket: BucketModel) {
    dispatch({
        type: BucketsActionTypes.SET_ACTIVE_BUCKET,
        bucket
    });

    const bucketContent = await BucketContentServices.getBucketContent(bucket);
    dispatch(FoldersActions.openBucketFolder(bucketContent));
}

async function getBuckets(dispatch: Dispatch<BucketsAction>) {
    const buckets = await BucketServices.getBuckets();

    dispatch({
        type: BucketsActionTypes.SET_BUCKETS,
        buckets: List(buckets)
    });
}

async function createBucket(dispatch: Dispatch<BucketsAction>, bucketName: string): Promise<string | undefined> {
    const createBucketError = await BucketServices.createBucket(bucketName);
    if (createBucketError) return createBucketError;

    const bucketToAdd = new BucketModel(bucketName);
    dispatch({
        type: BucketsActionTypes.ADD_BUCKET,
        bucket: bucketToAdd
    });

    return Promise.resolve(undefined);
}

export default {
    openBucket,
    getBuckets,
    createBucket
};