import { List } from 'immutable';
import { when } from 'jest-when';
import BucketErrors from '../../models/BucketErrors';
import BucketServices from '../../services/bucket/BucketServices';
import BucketContentServices from '../../services/bucketContent/BucketContentServices';
import BucketModelObjectMother from '../../tests/objectmothers/models/BucketModelObjectMother';
import FolderModelObjectMother from '../../tests/objectmothers/models/FolderModelObjectMother';
import BucketsActionTypes from '../types/BucketsActionTypes';
import FoldersActionTypes from '../types/FoldersActionTypes';
import BucketsActions from './BucketsActions';

describe('BucketsActions tests', () => {
    let dispatch: jest.Mock;

    beforeEach(() => {
        dispatch = jest.fn();
    });

    describe('openBucket', () => {
        it('sets active bucket, sets bucket content.', async () => {
            // ARRANGE
            const BUCKET = BucketModelObjectMother.get();
            const BUCKET_CONTENT = FolderModelObjectMother.get();

            BucketContentServices.getBucketContent = jest.fn().mockReturnValue(BUCKET_CONTENT);
            when(BucketContentServices.getBucketContent as any)
                .calledWith(BUCKET)
                .mockReturnValue(BUCKET_CONTENT);

            // ACT
            await BucketsActions.openBucket(dispatch, BUCKET);

            // ASSERT
            expect(dispatch).toHaveBeenCalledWith({
                type: BucketsActionTypes.SET_ACTIVE_BUCKET,
                bucket: BUCKET
            });

            expect(dispatch).toHaveBeenCalledWith({
                type: FoldersActionTypes.OPEN_BUCKET_FOLDER,
                folder: BUCKET_CONTENT
            });

            expect(BucketContentServices.getBucketContent).toHaveBeenCalledWith(BUCKET);
        });
    });

    describe('getBuckets', () => {
        it('setBuckets from services', async () => {
            // ARRANGE
            const BUCKET_A = BucketModelObjectMother.get('bucketA');
            const BUCKET_B = BucketModelObjectMother.get('bucketB');

            BucketServices.getBuckets = jest.fn().mockReturnValue(Promise.resolve([BUCKET_A, BUCKET_B]));
            when(BucketServices.getBuckets as any)
                .calledWith()
                .mockReturnValue(Promise.resolve([BUCKET_A, BUCKET_B]));

            // ACT
            await BucketsActions.getBuckets(dispatch);

            // ASSERT
            expect(dispatch).toHaveBeenCalledWith({
                type: BucketsActionTypes.SET_BUCKETS,
                buckets: List([BUCKET_A, BUCKET_B])
            });
        });
    });

    describe('createBucket', () => {
        it('When bucket is created with no error, a dispatch of bucket creation is done', async () => {
            // ARRANGE
            const createdBucket = BucketModelObjectMother.get('new-bucket');

            BucketServices.createBucket = jest.fn();
            when(BucketServices.createBucket as any)
                .calledWith('new-bucket')
                .mockReturnValue(Promise.resolve(undefined));

            // ACT
            const errorMessage = await BucketsActions.createBucket(dispatch, 'new-bucket');

            // ASSERT
            expect(errorMessage).toBeUndefined();

            expect(dispatch).toHaveBeenCalledWith({
                type: BucketsActionTypes.ADD_BUCKET,
                bucket: createdBucket
            });
        });

        it('When bucket is created with error, error message is returned and no bucket added', async () => {
            // ARRANGE
            BucketServices.createBucket = jest.fn();
            when(BucketServices.createBucket as any)
                .calledWith('new-bucket')
                .mockReturnValue(Promise.resolve(BucketErrors.BUCKET_ALREADY_EXISTS));

            // ACT
            const errorMessage = await BucketsActions.createBucket(dispatch, 'new-bucket');

            // ASSERT
            expect(errorMessage).toEqual(BucketErrors.BUCKET_ALREADY_EXISTS);

            expect(dispatch).not.toHaveBeenCalled();
        });
    });
});