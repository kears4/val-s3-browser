import FileModel from '../../../models/FileModel';
import FolderModel from '../../../models/FolderModel';

function get(name: string = 'folder'): FolderModel {
    return new FolderModel([], name);
}

function getWithContent(content: (FileModel | FolderModel)[]): FolderModel {
    return new FolderModel(content, 'name');
}

export default {
    get,
    getWithContent
};
