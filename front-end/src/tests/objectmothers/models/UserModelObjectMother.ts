import IndividuModel from '../../../models/UserModel';

function get(idValeria = 'vkv4',
    adresseCourriel = 'courriel@gmail.com',
    prenom = 'Linda',
    nom = 'Dumont',
    idRoleValeria = 1): IndividuModel {

    return new IndividuModel(idValeria, adresseCourriel, prenom, nom, idRoleValeria);
}

export default {
    get
};