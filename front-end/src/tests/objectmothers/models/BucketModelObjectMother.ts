import BucketModel from '../../../models/BucketModel';

function get(name: string = 'bucket'): BucketModel {
    return new BucketModel(name);
}

export default {
    get
};
