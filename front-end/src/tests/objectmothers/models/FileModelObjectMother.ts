import FileModel from '../../../models/FileModel';

function get(name: string = 'file', path: string = 'folder/file'): FileModel {
    return new FileModel(name, path);
}

export default {
    get
};
