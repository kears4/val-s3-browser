import RoleModel from '../../../models/RoleModel';

function get(idRole = 1,
    codeRole = 'ADMIN',
    nom = 'Administrateur'): RoleModel {

    return new RoleModel(idRole, codeRole, nom);
}

export default {
    get
};