import RoleModel from '../../../models/RoleModel';
import RolesState from '../../../reducers/roles/RolesState';

function get() {
    const rolesState = new RolesState();

    return rolesState;
}

function getWithRoles(roles: RoleModel[]) {
    const rolesState = new RolesState();

    roles.forEach((r) => {
        rolesState.roles = rolesState.roles.push(r);
    });

    return rolesState;
}

export default {
    get,
    getWithRoles,
};