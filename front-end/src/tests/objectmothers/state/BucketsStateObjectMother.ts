import BucketModel from '../../../models/BucketModel';
import BucketsState from '../../../reducers/buckets/BucketsState';

function get() {
    const bucketsState = new BucketsState();

    return bucketsState;
}

function getWithBuckets(buckets: BucketModel[]) {
    const bucketsState = new BucketsState();

    buckets.forEach((b) => {
        bucketsState.buckets = bucketsState.buckets.push(b);
    });

    return bucketsState;
}

function getWithActiveBucket(bucket: BucketModel) {
    const bucketsState = new BucketsState();

    bucketsState.activeBucket = bucket;
    return bucketsState;
}

export default {
    get,
    getWithBuckets,
    getWithActiveBucket
};