import FolderModel from '../../../models/FolderModel';
import FoldersState from '../../../reducers/folders/FoldersState';

function get() {
    return new FoldersState();
}

function getWithPath(path: FolderModel[]) {
    const foldersState = new FoldersState();

    path.forEach(pi => {
        foldersState.path = foldersState.path.push(pi);
    });

    return foldersState;
}

export default {
    get,
    getWithPath
};