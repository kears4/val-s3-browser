import UserModel from '../../../models/UserModel';
import UserState from '../../../reducers/user/UserState';

function get() {
    return new UserState();
}

function getWithUserModel(userModel: UserModel) {
    const userState = new UserState();
    userState.user = userModel;

    return userState;
}

export default {
    get,
    getWithUserModel
};