import BucketsState from '../../../reducers/buckets/BucketsState';
import FoldersState from '../../../reducers/folders/FoldersState';
import RolesState from '../../../reducers/roles/RolesState';
import UserState from '../../../reducers/user/UserState';
import State from '../../../State';
import BucketsStateObjectMother from './BucketsStateObjectMother';
import FoldersStateObjectMother from './FoldersStateObjectMother';
import RolesStateObjectMother from './RolesStateObjectMother';
import UserStateObjectMother from './UserStateObjectMother';

function get(): State {
    return {
        folders: FoldersStateObjectMother.get(),
        buckets: BucketsStateObjectMother.get(),
        user: UserStateObjectMother.get(),
        roles: RolesStateObjectMother.get(),
    };
}

function getWithFoldersState(foldersState: FoldersState): State {
    return {
        folders: foldersState,
        buckets: BucketsStateObjectMother.get(),
        user: UserStateObjectMother.get(),
        roles: RolesStateObjectMother.get(),
    };
}

function getWithBucketsState(bucketsState: BucketsState): State {
    return {
        folders: FoldersStateObjectMother.get(),
        buckets: bucketsState,
        user: UserStateObjectMother.get(),
        roles: RolesStateObjectMother.get(),
    };
}

function getWithUserState(userState: UserState): State {
    return {
        folders: FoldersStateObjectMother.get(),
        buckets: BucketsStateObjectMother.get(),
        user: userState,
        roles: RolesStateObjectMother.get(),
    };
}

function getWithUserAndRolesState(userState: UserState, roles: RolesState): State {
    return {
        folders: FoldersStateObjectMother.get(),
        buckets: BucketsStateObjectMother.get(),
        user: userState,
        roles,
    };
}

export default {
    get,
    getWithFoldersState,
    getWithBucketsState,
    getWithUserState,
    getWithUserAndRolesState
};