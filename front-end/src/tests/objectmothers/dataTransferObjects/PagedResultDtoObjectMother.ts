import PagedResultDto from '../../../dataTransferObjects/PagedResultDto';

function get<TResultType>(result: TResultType): PagedResultDto<TResultType> {
    return {
        resultats: [result]
    };
}

function getWithResults<TResultType>(results: TResultType[]): PagedResultDto<TResultType> {
    return {
        resultats: results
    };
}

export default {
    get,
    getWithResults
};