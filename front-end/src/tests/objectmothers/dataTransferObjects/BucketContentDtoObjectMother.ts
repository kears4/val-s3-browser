import BucketContentDto from '../../../dataTransferObjects/BucketContentDto';

function get(path: string = 'folder/file.ext'): BucketContentDto {
    return {
        path
    };
}

export default {
    get
};
