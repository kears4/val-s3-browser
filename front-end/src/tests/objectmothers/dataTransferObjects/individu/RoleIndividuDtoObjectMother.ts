import RoleIndividuDto from '../../../../dataTransferObjects/individu/RoleIndividuDto';

function get(idRole = 1): RoleIndividuDto {
    return {
        idRole
    };
}

export default {
    get
};