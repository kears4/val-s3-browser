import IndividuDto from '../../../../dataTransferObjects/individu/IndividuDto';
import RoleIndividuDtoObjectMother from './RoleIndividuDtoObjectMother';

function get(idValeria = 'vkv4',
    adresseCourriel = 'courriel@gmail.com',
    prenom = 'Maxime',
    nom = 'Lamontagne'): IndividuDto {

    return {
        idValeria,
        adresseCourriel,
        prenom,
        nom,
        rolesIndividu: [RoleIndividuDtoObjectMother.get()]
    };
}

export default {
    get
};