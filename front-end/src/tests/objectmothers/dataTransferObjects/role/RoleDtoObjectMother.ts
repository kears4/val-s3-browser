import RoleDto from '../../../../dataTransferObjects/role/RoleDto';

function get(idRole = 1, codeRole = 'ADMIN', nom = 'Administrateur'): RoleDto {
    return {
        idRole,
        codeRole,
        nom
    };
}

export default {
    get
};