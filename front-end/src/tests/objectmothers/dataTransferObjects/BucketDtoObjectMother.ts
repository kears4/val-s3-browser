import BucketDto from '../../../dataTransferObjects/BucketDto';

function get(name: string = 'bucket'): BucketDto {
    return {
        name
    };
}

export default {
    get
};
