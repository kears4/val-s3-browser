import BucketsState from './reducers/buckets/BucketsState';
import FoldersState from './reducers/folders/FoldersState';
import RolesState from './reducers/roles/RolesState';
import UserState from './reducers/user/UserState';

export default interface State {
    folders: FoldersState;
    buckets: BucketsState;
    user: UserState;
    roles: RolesState;
}
