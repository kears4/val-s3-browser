export default interface AppProps {
    logout: () => void;
}