function downloadUrl(url: string) {
    window.location.assign(url);
}

export default {
    downloadUrl
};