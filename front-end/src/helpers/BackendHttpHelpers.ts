import Axios from 'axios';
import EnvironnementVariables from '../EnvironnementVariables';

function get(url: string, token: string) {
    const headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
    };

    return Axios.get(getUrl(url), { headers }).catch(generalErrorHandler(url));
}

function post(url: string, token: string, data?: any) {
    const headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
    };

    return Axios.post(getUrl(url), data, { headers });
}

function generalErrorHandler(url: string) {
    return (error: any) => {
        alert(`Failed to execute url ${url} with error: ${error} `);
        return Promise.reject(error);
    };
}

function getUrl(url: string) {
    return `${EnvironnementVariables.backendUrl}/${url}`;
}

export default {
    get,
    post,
};
