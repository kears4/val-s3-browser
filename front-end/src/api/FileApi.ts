import BackendHttpHelpers from '../helpers/BackendHttpHelpers';
import TokenProvider from '../TokenProvider';
import Axios from 'axios';


async function getDownloadUrl(bucketName: string, fileKey: string): Promise<string> {
    const token = TokenProvider.getUserToken();
    const url = `file/download-file-url?bucket=${bucketName}&file_key=${fileKey}`;
    const data = await (await BackendHttpHelpers.get(url, token)).data;

    return Promise.resolve(data);
}

async function getUploadUrl(bucketName: string, fileKey: string): Promise<string> {
    const token = TokenProvider.getUserToken();
    const url = `file/upload-file-url?bucket=${bucketName}&file_key=${fileKey}`;
    const data = await (await BackendHttpHelpers.get(url, token)).data;

    return Promise.resolve(data);
}

// TODO: Rendu à déboguer cors origin pour s3 at.
async function uploadFileToS3(signedUploadUrl: string, file: FormData): Promise<string> {
    const response = await Axios.put(signedUploadUrl, file, {
        headers: {
            'Accept': 'application/octet-stream',
            'Content-Type': 'application/octet-stream'
        }
    });

    return Promise.resolve(response.data);
}

export default {
    getDownloadUrl,
    getUploadUrl,
    uploadFileToS3
};