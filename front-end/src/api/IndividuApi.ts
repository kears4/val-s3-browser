import IndividuDto from '../dataTransferObjects/individu/IndividuDto';
import PagedResult from '../dataTransferObjects/PagedResultDto';
import ValServicesHttpHelpers from '../helpers/ValServicesHttpHelpers';
import TokenProvider from '../TokenProvider';

async function getIndividu(): Promise<PagedResult<IndividuDto>> {
    const token = TokenProvider.getUserToken();

    const data = await
        (await ValServicesHttpHelpers.get('v1/individus?referencesajoutees=RolesIndividu', token)).data;
    return Promise.resolve(data);
}

export default {
    getIndividu
};