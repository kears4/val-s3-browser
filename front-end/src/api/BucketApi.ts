import BucketDto from '../dataTransferObjects/BucketDto';
import BackendHttpHelpers from '../helpers/BackendHttpHelpers';
import HttpStatusCodes from '../helpers/HttpStatusCodes';
import TokenProvider from '../TokenProvider';

async function getBuckets(): Promise<BucketDto[]> {
    const token = TokenProvider.getUserToken();
    const data = await (await BackendHttpHelpers.get('bucket/', token)).data as BucketDto[];

    return Promise.resolve(data);
}

async function createBucket(bucketName: string): Promise<number> {
    const token = TokenProvider.getUserToken();
    try {
        const response = await BackendHttpHelpers.post(`bucket/create/${bucketName}`, token);
        return response.status;
    } catch (error) {
        console.error(error);
        return Promise.resolve(HttpStatusCodes.SERVER_ERROR);
    }
}

export default {
    getBuckets,
    createBucket
};