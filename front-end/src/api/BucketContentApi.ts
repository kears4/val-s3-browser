import BucketContentDto from '../dataTransferObjects/BucketContentDto';
import BackendHttpHelpers from '../helpers/BackendHttpHelpers';
import TokenProvider from '../TokenProvider';

async function getBucketContent(bucketName: string): Promise<BucketContentDto[]> {
    const uri = `bucket/content/${bucketName}`;
    const token = TokenProvider.getUserToken();

    const data = await (await BackendHttpHelpers.get(uri, token)).data as BucketContentDto[];

    return Promise.resolve(data);
}

export default {
    getBucketContent
};