import PagedResult from '../dataTransferObjects/PagedResultDto';
import RoleDto from '../dataTransferObjects/role/RoleDto';
import ValServicesHttpHelpers from '../helpers/ValServicesHttpHelpers';
import TokenProvider from '../TokenProvider';

async function getRoles(): Promise<PagedResult<RoleDto>> {
    const token = TokenProvider.getUserToken();

    const data = await
        (await ValServicesHttpHelpers.get('v1/roles/', token)).data;
    return Promise.resolve(data);
}

export default {
    getRoles
};