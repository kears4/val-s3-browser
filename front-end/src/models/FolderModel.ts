import FileModel from './FileModel';

export default class FolderModel {
    content: (FileModel | FolderModel)[];
    name: string;

    constructor(content: (FileModel | FolderModel)[], name: string) {
        this.content = content;
        this.name = name;
    }
}