export default class RoleModel {
    idRole: number;
    codeRole: string;
    nom: string;

    constructor(idRole: number,
        codeRole: string,
        nom: string) {

        this.idRole = idRole;
        this.codeRole = codeRole;
        this.nom = nom;
    }
}