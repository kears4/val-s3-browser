export default class IndividuModel {
    valeriaId: string;
    email: string;
    firstName: string;
    lastName: string;
    roleId: number;

    constructor(valeriaId: string,
        email: string,
        firstName: string,
        lastName: string,
        roleId: number) {

        this.valeriaId = valeriaId;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;

        this.roleId = roleId;
    }
}