export default class BucketModel {
    name: string;

    constructor(name: string) {
        this.name = name;
    }
}