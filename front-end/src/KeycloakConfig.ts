export default {
    realm: 'valeria',
    url: 'https://auth.at.valeria.science/auth/',
    clientId: 'ul-val-s3-browser',
    publicClient: true
};