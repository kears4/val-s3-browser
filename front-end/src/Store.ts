import { Action, applyMiddleware, combineReducers, compose, createStore, Store } from 'redux';
import Thunk from 'redux-thunk';

import State from './State';
import ActionTypes from './actions/types/ActionTypes';
import foldersReducer from './reducers/folders/FoldersReducer';
import bucketsReducer from './reducers/buckets/BucketsReducer';
import userReducer from './reducers/user/UserReducer';
import rolesReducer from './reducers/roles/RolesReducer';

const songApp = combineReducers<State>({
    folders: foldersReducer,
    buckets: bucketsReducer,
    user: userReducer,
    roles: rolesReducer
});

const rootReducer = (state: State | undefined, action: Action): State => {
    if (action?.type === ActionTypes.RESET) {
        state = undefined;
    }

    return songApp(state, action);
};

const composeEnhancers =
    typeof window === 'object' &&
        // @ts-ignore
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        // @ts-ignore
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const enhancer = composeEnhancers(
    // applyMiddleware(Thunk, Logger),
    applyMiddleware(Thunk),
);

const store: Store<State> = createStore<State, any, any, any>(rootReducer, enhancer);

export default store;
